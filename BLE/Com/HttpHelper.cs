﻿using lingopen.Foxconn.BLE.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace lingopen.Foxconn.BLE.Com
{
    /// <summary>
    /// 跨域http访问公共类
    /// </summary>
    public class HttpHelper
    {
        /// <summary>
        /// 跨域访问
        /// </summary>
        /// <param name="url"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string POST(string url, string param, string contentType = "x-www-form-urlencoded", string basicAuthorization = null, int time = 60000)
        {
            $"Http POST:url={url},param={param}".Log();

            Uri address = new Uri(url);
            // string authorization = Convert.ToBase64String(new ASCIIEncoding().GetBytes(cfg.UserName + ":" + cfg.Password));
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            if (basicAuthorization.IsNotNull())
                request.Headers.Add("Authorization", "Basic " + basicAuthorization);  //计算和下面的 密码不匹配
            //request.Headers.Add("Authorization", "Basic c3VubGlhbmc6c2Fwc2FwMTIz"); 
            request.ContentType = $"application/{contentType};charset=utf-8"; //"application/x-www-form-urlencoded";;charset=utf-8
            request.Timeout = time;
            byte[] byteData = new ASCIIEncoding().GetBytes(param ?? "");// UTF8Encoding.UTF8.GetBytes(param == null ? "" : param);
            request.ContentLength = byteData.Length;


            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }


            string result = "";
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                result = reader.ReadToEnd();
            }
            return (result);


        }

        /// <summary>
        /// 跨域访问
        /// </summary>
        /// <param name="url"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string GET(string url, string contentType = "x-www-form-urlencoded", int time = 60000)
        {
            Uri address = new Uri(url);
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "GET";
            request.ContentType = $"application/{contentType};charset=utf-8"; //"application/x-www-form-urlencoded";
            request.Timeout = time;
            string result = "";
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                result = reader.ReadToEnd();
            }
            return (result);
        }
    }
}