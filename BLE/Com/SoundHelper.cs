﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace lingopen.Foxconn.BLE.Com
{
    public class SoundHelper
    {
        private static string strFilePath;

        public static string FilePath
        {
            get
            {
                return SoundHelper.strFilePath;
            }
            set
            {
                if (!value.Contains(".wav"))
                    value += ".wav";
                SoundHelper.strFilePath = value;
            }
        }

        public SoundHelper()
        {
        }

        public SoundHelper(string strPath)
        {
            if (!strPath.Contains(".wav"))
                strPath += ".wav";
            SoundHelper.strFilePath = strPath;
        }

        [DllImport("winmm")]
        public static extern bool PlaySound(string szSound, IntPtr hMod, int i);

        public static bool PlayMySound()
        {
            return SoundHelper.PlaySound(SoundHelper.strFilePath, IntPtr.Zero, 1);
        }

        public static bool PlayMySound(string path)
        {
            return SoundHelper.PlaySound(path, IntPtr.Zero, 1);
        }
    }
}
