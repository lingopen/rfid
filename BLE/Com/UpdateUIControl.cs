﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lingopen.Foxconn.BLE.Com
{
    /// <summary>
    /// UI线程安全
    /// </summary>
    public static class UpdateUI
    {
        /// <summary>
        /// 子线程对UI线程控件的控制
        /// </summary>
        /// <param name="control"></param>
        /// <param name="action"></param>
        /// <param name="param"></param>
        public static void UpdateUIControl(Control control, Action action, params object[] param)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new UpdateUIEventHandler(UpdateUIControl), param);
            }
            else
            {
                action?.Invoke();
            }

        }

    }
    public delegate void UpdateUIEventHandler(Control control, Action action, params object[] param);

}
