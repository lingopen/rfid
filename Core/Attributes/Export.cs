﻿using Core.Enums;
using Core.Interfaces;
using System;
using System.ComponentModel.Composition;

namespace Core.Attributes
{
    /// <summary>
    /// 扩展程序特性，用于将程序类标记为扩展程序
    /// </summary>
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ExportIotAttribute : ExportAttribute, IIotData
    {
        /// <summary>
        /// 隐藏默认构造函数
        /// </summary>
        private ExportIotAttribute()
        {
        }
        /// <summary>
        /// IOT设备描述
        /// </summary>
        /// <param name="type">类型</param>
        /// <param name="name">名称</param>
        /// <param name="displayName">显示名</param>
        /// <param name="description">描述</param>
        public ExportIotAttribute(IotType type, string name, string displayName, string description) :
            base(typeof(IIot))
        {
            Name = name;
            Type = type;
            DisplayName = displayName;
            Description = description;
        }

        public string Name { get; } = "";

        public IotType Type { get; } = IotType.None;

        public string DisplayName { get; } = "";
        public string Description { get; } = "";
    }
}
