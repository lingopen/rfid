﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Com
{
    public static class Extention
    {

        /// <summary>
        /// 转换为json格式
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public static string ToJson(this object entity, NullValueHandling nullValueHandling= NullValueHandling.Ignore)
        {
            return JsonConvert.SerializeObject(entity, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings { NullValueHandling = nullValueHandling });
        }
        /// <summary>
        /// 转换为对象 T
        /// </summary>
        /// <typeparam name="T">返回值类型</typeparam>
        /// <param name="json">json数据</param>
        /// <returns></returns>
        public static T FromJson<T>(this string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
        /// <summary>
        /// 是否不为空
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNotNull(this string str)
        {
            return !string.IsNullOrEmpty(str);
        }

        public static bool ListIsNull<T>(this List<T> list)
        {
            return list == null || list.Count < 1;
        }
        public static bool IsNull(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool IsNotNull<T>(this IList<T> list)
        {
            return list != null && list.Count > 0;
        }
    }
}