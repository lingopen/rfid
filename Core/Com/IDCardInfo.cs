﻿using System;

namespace Core.Com
{
    /// <summary>
    /// 身份证信息
    /// </summary>
    public class IDCardInfo : IComparable
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// 出生日期
        /// </summary>
        public string Born { get; set; }
        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime BornDate
        {
            get
            {
                if (String.IsNullOrEmpty(Born))
                {
                    if (!String.IsNullOrEmpty(CardNo) && CardNo.Length == 18)
                    {
                        Born = CardNo.Substring(6, 8);
                    }
                }
                if (!String.IsNullOrEmpty(Born))
                {
                    var born = Born + "";
                    if (born.Length == 8)
                    {
                        born = born.Insert(6, "-");
                        born = born.Insert(4, "-");
                    }
                    return Convert.ToDateTime(born);
                }
                return DateTime.Parse("2000-01-01");
            }
        }
        /// <summary>
        /// 民族
        /// </summary>
        public string Folk { get; set; }
        /// <summary>
        /// 发证机关
        /// </summary>
        public string Agency { get; set; }
        /// <summary>
        /// 有效期起始日期
        /// </summary>
        public string ExpireStart { get; set; }
        /// <summary>
        /// 有效期起始日期
        /// </summary>
        public DateTime ExpireStartDate
        {
            get
            {
                if (!String.IsNullOrEmpty(ExpireStart))
                {
                    var expireStart = ExpireStart + "";
                    if (expireStart.Length == 8)
                    {
                        expireStart = expireStart.Insert(6, "-");
                        expireStart = expireStart.Insert(4, "-");
                    }
                    var date = DateTime.Now;
                    if (!DateTime.TryParse(expireStart, out date))
                    {
                        date = new DateTime(DateTime.Now.Year / 100 * 100, 1, 1);
                    }
                    return date;
                }
                return DateTime.Parse("2000-01-01");
            }
        }
        /// <summary>
        /// 有效期截止日期
        /// </summary>
        public string ExpireEnd { get; set; }
        /// <summary>
        /// 有效期截止日期
        /// </summary>
        public DateTime ExpireEndDate
        {
            get
            {
                try
                {
                    if (!String.IsNullOrEmpty(ExpireEnd))
                    {
                        var expireEnd = ExpireEnd + "";
                        if (expireEnd.Length == 8)
                        {
                            expireEnd = expireEnd.Insert(6, "-");
                            expireEnd = expireEnd.Insert(4, "-");
                        }
                        var date = DateTime.Now;
                        if (!DateTime.TryParse(expireEnd, out date))
                        {
                            date = new DateTime((DateTime.Now.Year + 1) / 100 * 100 + 99, 1, 1);
                        }
                        return date;
                    }
                }
                catch//长期
                {

                }
                return DateTime.Parse("3999-01-01");
            }
        }
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 身份证头像
        /// </summary>
        public byte[] Avatar { get; set; }
        /// <summary>
        /// 正面图片
        /// </summary>
        public byte[] ImageA { get; set; }
        /// <summary>
        /// 背面图片
        /// </summary>
        public byte[] ImageB { get; set; }
        /// <summary>
        /// 实景照片
        /// </summary>
        public byte[] Photo { get; set; }
        /// <summary>
        /// 实景照片日期
        /// </summary>
        public DateTime? PhotoDate { get; set; }
        /// <summary>
        /// 实景面部相似度
        /// </summary>
        public double FaceConfidence { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            return (CardNo + "").CompareTo(((IDCardInfo)obj).CardNo + "");
        }
    }
}