﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Com
{
    /// <summary>
    /// 系统配置信息
    /// </summary>
    public class SystemConfig
    {
        [Newtonsoft.Json.JsonIgnore]
        public Func<string, string> GetSettingFunc = null;

        public string Reader { get; set; }
        /// <summary>
        /// 主摄像头
        /// </summary>
        public string CameraName { get; set; }
        /// <summary>
        /// 辅助摄像头
        /// </summary>
        public string VcameraName { get; set; }
        /// <summary>
        /// 是否加载时便打开授权按钮
        /// </summary>
        public bool QRAuthLoadOpen { get; set; }
        /// <summary>
        /// 识别通过率
        /// </summary>
        public int? FaceConfidence { get; set; }
        public int AllowQRAuthFaceCompare { get; set; }
        public string QRScanerPort { get; set; }
        public string ScreenSize { get; set; } = "Screen32";
        public string RestartTime { get; set; }
        public string MainLogo { get; set; }
        public string BackgroundPic { get; set; }

        public string GetSetting(string name)
        {
            if (GetSettingFunc == null) { return null; }
            return GetSettingFunc?.Invoke(name);
        }
    }
}
