﻿using Core.Enums;
using Core.Interfaces;
using System.Linq;

namespace Core.Com
{
    public class SystemIots
    { /// <summary>
      /// 获取扩展对象
      /// </summary>
      /// <typeparam name="T">继承自IExtention的接口</typeparam>
      /// <param name="type">接口类型</param>
      /// <param name="name">接口名</param>
      /// <returns></returns>
        private T GetIot<T>(IotType type, string name)
        {
            var extention = SystemContext.Current.Iots.FirstOrDefault(o => o.Metadata.Type == type && o.Metadata.Name == name);
            if (extention != null && extention.Value != null && extention.Value is T)
            {
                return (T)extention.Value;
            }
            return default(T);
        }
        /// <summary>
        /// 环境检测
        /// </summary>
        public IReader Reader
        {
            get
            {
                return GetIot<IReader>(IotType.Reader, SystemContext.Current.Config.Reader);
            }
        }
    }
}