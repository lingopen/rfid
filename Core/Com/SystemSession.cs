﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Com
{
    public class SystemSession
    {
        /// <summary>
        /// 会话ID
        /// </summary>
        public string SessionId { get; } = Guid.NewGuid().ToString();
        /// <summary>
        /// 用于缓存查询订单的身份证信息，和订单所有客人的身份证信息缓存分开
        /// </summary>
        public IDCardInfo QueryIDCardInfo { get; set; }
        public List<IDCardInfo> CheckIdCardInfo { get; set; }
    }
}
