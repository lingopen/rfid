﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Com
{
    /// <summary>
    /// 系统设置
    /// </summary>
    public class SystemSettings
    {
        /// <summary>
        /// JSON序列化设置
        /// </summary>
        public JsonSerializerSettings JSONSerializeSettings
        {
            get
            {
                return new JsonSerializerSettings()
                {
                    DateFormatString = "yyyy-MM-dd HH:mm:ss",
                    NullValueHandling = NullValueHandling.Ignore
                };
            }
        }

        ///// <summary>
        ///// 网易云信APP Key
        ///// </summary>
        //public string NIMAppKey
        //{
        //    get
        //    {
        //        return "0d1810f2a38d54b71c6021d44ca84776";
        //    }
        //}
    }
}
