﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enums
{
    /// <summary>
    /// 状态
    /// </summary>
    public enum IotState
    {
        None,
        ON,
        OFF,
    }
}
