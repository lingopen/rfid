﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enums
{
    /// <summary>
    /// 物联网设备类型
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum IotType
    { 
        /// <summary>
        /// 阅读器
        /// </summary>
        Reader,
        /// <summary>
        /// 无
        /// </summary>
        None,
    }
}
