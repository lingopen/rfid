﻿using Core.Base;
using Core.Enums;
using System;

namespace Core.Interfaces
{
    /// <summary>
    /// 物联网设备基接口
    /// </summary>
    public interface IIot
    {
        ///// <summary>
        ///// 设备开机，初始化，加载配置等
        ///// </summary>
        ///// <param name="callback">回调</param>
        ///// <returns></returns>
        //bool PowerON(Action<string> callback);
        ///// <summary>
        ///// 设备关机，清除缓存，端口资源等
        ///// </summary>
        ///// <returns></returns>
        //bool PowerOFF();
        ///// <summary>
        ///// 设备状态
        ///// </summary>
        //IotState IotState { get; set; }
        DockContentEx GetApp();
        /// <summary>
        /// 获取名称
        /// </summary>
        /// <returns></returns>
        string GetName();

        ///// <summary>
        ///// 启动设备
        ///// </summary>
        ///// <returns></returns>
        //bool Start();
        ///// <summary>
        ///// 停止设备
        ///// </summary>
        ///// <returns></returns>
        //bool Stop();

    }
}
