﻿using Core.Enums;

namespace Core.Interfaces
{
    /// <summary>
    /// IOT描述信息
    /// </summary>
    public interface IIotData
    {
        /// <summary>
        /// 保存在配置文件中的名称，同类型的要求唯一
        /// </summary>
        string Name { get; }
        /// <summary>
        /// IOT类型
        /// </summary>
        IotType Type { get; }
        ///// <summary>
        ///// MAC地址
        ///// </summary>
        //string MAC { get; set; }
        ///// <summary>
        ///// IP地址
        ///// </summary>
        //string IP { get; set; }
        ///// <summary>
        ///// 端口号
        ///// </summary>
        //int Port { get; set; }
        ///// <summary>
        ///// COM端口号
        ///// </summary>
        //string COMPort { get; set; }
        /// <summary>
        /// 显示名称
        /// </summary>
        string DisplayName { get; }
        /// <summary>
        /// 描述信息
        /// </summary>
        string Description { get; }
    }
}
