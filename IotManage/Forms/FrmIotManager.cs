﻿using Core.Com;
using Core.Enums;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace IotManage.Forms
{
    public partial class FrmIotManager : Form
    {

        FrmMenu frmMenu = null;
        public FrmIotManager()
        {
            InitializeComponent();
        }
        private void FrmIotManager_Load(object sender, EventArgs e)
        {
            frmMenu = new FrmMenu();
            frmMenu.IotManager += FrmMenu_IotManager;
            frmMenu.Show(mdi, DockState.DockLeft);
            frmMenu.DockPanel.DockLeftPortion = 0.15;
        }

        private bool FrmMenu_IotManager(IIot app, string action)
        {

            switch (action)
            {
                //case "start":
                //    {
                //        return app.Start();
                //    }
                //case "stop":
                //    { return app.Stop(); }
                case "show":
                default:
                    {
                        app.GetApp().Show(mdi);
                        
                        return true;
                    }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
