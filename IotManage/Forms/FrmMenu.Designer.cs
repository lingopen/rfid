﻿namespace IotManage.Forms
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("RFID");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("BLE");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Reader", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Other");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenu));
            this.treeIot = new System.Windows.Forms.TreeView();
            this.listImg = new System.Windows.Forms.ImageList(this.components);
            this.ctms = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnView = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStart = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStop = new System.Windows.Forms.ToolStripMenuItem();
            this.ctms.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeIot
            // 
            this.treeIot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeIot.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.treeIot.ImageIndex = 0;
            this.treeIot.ImageList = this.listImg;
            this.treeIot.Indent = 24;
            this.treeIot.ItemHeight = 24;
            this.treeIot.Location = new System.Drawing.Point(0, 0);
            this.treeIot.Name = "treeIot";
            treeNode1.ImageIndex = 2;
            treeNode1.Name = "节点2";
            treeNode1.Text = "RFID";
            treeNode2.ImageIndex = 1;
            treeNode2.Name = "节点3";
            treeNode2.Text = "BLE";
            treeNode3.Name = "节点0";
            treeNode3.Text = "Reader";
            treeNode4.Name = "节点1";
            treeNode4.Text = "Other";
            this.treeIot.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode4});
            this.treeIot.SelectedImageIndex = 0;
            this.treeIot.Size = new System.Drawing.Size(392, 633);
            this.treeIot.TabIndex = 0;
            this.treeIot.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treeIot_MouseUp);
            // 
            // listImg
            // 
            this.listImg.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("listImg.ImageStream")));
            this.listImg.TransparentColor = System.Drawing.Color.Transparent;
            this.listImg.Images.SetKeyName(0, "type.ico");
            this.listImg.Images.SetKeyName(1, "stop (2).png");
            this.listImg.Images.SetKeyName(2, "running.png");
            // 
            // ctms
            // 
            this.ctms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnView,
            this.btnStart,
            this.btnStop});
            this.ctms.Name = "ctms";
            this.ctms.Size = new System.Drawing.Size(181, 92);
            // 
            // btnView
            // 
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(180, 22);
            this.btnView.Text = "查看(&V)";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnStart
            // 
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(180, 22);
            this.btnStart.Text = "启动(&R)";
            this.btnStart.Visible = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Image = ((System.Drawing.Image)(resources.GetObject("btnStop.Image")));
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(180, 22);
            this.btnStop.Text = "停止(&S)";
            this.btnStop.Visible = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 633);
            this.CloseButton = false;
            this.Controls.Add(this.treeIot);
            this.HideOnClose = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMenu";
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.DockLeft;
            this.TabText = "子系统";
            this.Text = "子系统";
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            this.ctms.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeIot;
        private System.Windows.Forms.ImageList listImg;
        private System.Windows.Forms.ContextMenuStrip ctms;
        private System.Windows.Forms.ToolStripMenuItem btnStart;
        private System.Windows.Forms.ToolStripMenuItem btnStop;
        private System.Windows.Forms.ToolStripMenuItem btnView;
    }
}