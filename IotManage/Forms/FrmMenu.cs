﻿using Core.Base;
using Core.Com;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace IotManage.Forms
{
    public partial class FrmMenu : DockContent
    {
        /// <summary>
        /// 菜单点击事件
        /// </summary>
        public event IotManageEventHandler IotManager;
        public FrmMenu()
        {
            InitializeComponent();
        }

        private void FrmMenu_Load(object sender, EventArgs e)
        {
            var iots = SystemContext.Current.Iots;
            if (iots == null || iots.Count() <= 0) treeIot.Nodes.Clear();
            else
            {
                treeIot.Nodes.Clear();
                var types = iots.Select(p => p.Metadata.Type).Distinct();
                foreach (var type in types)
                {
                    TreeNode root = new TreeNode(type.ToString());
                    var childs = iots.Where(p => p.Metadata.Type == type && p.Value != null && p.Value is IReader).ToList();
                    foreach (var child in childs)
                    {
                        TreeNode app = new TreeNode();
                        app.Text = child.Metadata.DisplayName + $"  ({child.Metadata.Description})";

                        TreeNode node = new TreeNode();
                        node.ImageIndex = 1;
                        node.SelectedImageIndex = 1;
                        node.Tag = child.Value;
                        node.Text = "N/A";
                        node.Name = child.Metadata.Name;
                        node.ContextMenuStrip = this.ctms;
                        app.Nodes.Add(node);
                        root.Nodes.Add(app);
                    }
                    treeIot.Nodes.Add(root);
                }

                treeIot.ExpandAll();


            }

            //.Where(o => o.Metadata.Type == Core.Enums.IotType.Reader && o.Value != null && o.Value is IReader).Select(o => o.Metadata).ToList();

            //foreach (var item in list)
            //{

            //}
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (this.treeIot.SelectedNode.Text == "N/A")
            {
                return;
            }
            var iot = treeIot.SelectedNode.Tag as IIot;
            if (iot == null) return;
            bool? ret = IotManager?.Invoke(iot, "start");
            if ((bool)ret)
            {
                treeIot.SelectedNode.ImageIndex = 2;
                treeIot.SelectedNode.SelectedImageIndex = 2;
            }
            else
            {
                treeIot.SelectedNode.ImageIndex = 1;
                treeIot.SelectedNode.SelectedImageIndex = 1;
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (this.treeIot.SelectedNode.Text == "N/A")
            {
                return;
            }
            var iot = treeIot.SelectedNode.Tag as IIot;
            if (iot == null) return;
            var ret = IotManager?.Invoke(iot, "stop");
            if ((bool)ret)
            {
                treeIot.SelectedNode.ImageIndex = 1;
                treeIot.SelectedNode.SelectedImageIndex = 1;
            }
            else
            {
                treeIot.SelectedNode.ImageIndex = 2;
                treeIot.SelectedNode.SelectedImageIndex = 2;
            }
        }

        private void treeIot_MouseUp(object sender, MouseEventArgs e)
        {
            //根据鼠标在treeview点击的位置获取节点
            //TreeView控件，SelectNode属性更改只响应左键点击事件。右键点击，按下选择了节点。弹起，选择丢失。
            //实际工作中，经常需要面对的是右键直接选择，并弹出快捷菜单。故在鼠标弹起时，根据位置，设定SelectNode
            this.treeIot.SelectedNode = this.treeIot.GetNodeAt(e.X, e.Y);
           
            //if (e.Button == MouseButtons.Right)//仅响应右键
            //{
            //    //Node.Levle属性，确定是哪一级节点
            //    if (this.treeIot.SelectedNode != null && this.treeIot.SelectedNode.Level == 1)
            //    {
            //        //Show两个参数，Control和Point。
            //        //this指定相对的控件位置
            //        //Point为treeview点击的位置，20是为了显示ContextMenuStript时不遮挡Node，可以看到点击的Node
            //        this.ctms.Show(this, new Point(e.X + 20, e.Y));
            //    }
            //}

        }

        private void btnView_Click(object sender, EventArgs e)
        {
            var iot = treeIot.SelectedNode.Tag as IIot;
            if (iot == null) return;
            IotManager?.Invoke(iot, "show");
            this.treeIot.SelectedNode.Text = iot.GetName();
        }
    }
}