﻿using Core.Base;
using Core.Enums;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.BLE.Com
{
    [Core.Attributes.ExportIot(IotType.Reader, "BLE", "BLE", "园区保安员定位系统")]
    public class BLEReader : IReader 
    {
        static Forms.FrmBLE app = null;
        //public IotState IotState
        //{
        //    get { return app.IotState; }
        //    set { app.IotState = value; }
        //}

        public DockContentEx GetApp()
        {
            if (app == null)
                app = new Forms.FrmBLE();
            return app;
        }

        public string GetName()
        {
            if (app == null)
                app = new Forms.FrmBLE();
            return app.GetName();
        }

        //public bool Init(Action<string> callback)
        //{
        //    if (app == null)
        //        app = new Forms.FrmBLE();
        //    var ret = app.PowerON(callback);
        //    if (ret) IotState = IotState.ON;
        //    else IotState = IotState.OFF;
        //    return ret;
        //}

        //public bool PowerOFF()
        //{
        //    if (app == null)
        //        app = new Forms.FrmBLE();
        //    return app.PowerOFF();
        //}

        //public bool PowerON(Action<string> callback)
        //{
        //    if (app == null)
        //        app = new Forms.FrmBLE();
        //    var ret = app.PowerON(callback);
        //    if (ret) IotState = IotState.ON;
        //    else IotState = IotState.OFF;
        //    return ret;
        //}

        //public bool Start()
        //{
        //    if (app == null)
        //        app = new Forms.FrmBLE();
        //    return app.Start();
        //}

        //public bool Stop()
        //{
        //    if (app == null)
        //        app = new Forms.FrmBLE();
        //    return app.Stop();
        //}
    }
}
