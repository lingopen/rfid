﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.BLE.Models
{
    public class Device
    {
        public string IP { get; set; }
        public string Position { get; set; }
    }
}
