﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.BLE.Models
{
    public class Employee
    {
        public string EmployeeNo { get; set; }
        public string EmployeeName { get; set; }
        public string MAC { get; set; }
    }
}
