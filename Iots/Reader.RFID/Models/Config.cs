﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.RFID.Models
{


    //DescriptionAttribute - 设置显示在属性下方说明帮助窗格中的属性文本。这是一种为活动属性（即具有焦点的属性）提供帮助文本的有效方法。可以将此特性应用于 MaxRepeatRate 属性。
    //CategoryAttribute - 设置属性在网格中所属的类别。当您需要将属性按类别名称分组时，此特性非常有用。如果没有为属性指定类别，该属性将被分配给杂项 类别。可以将此特性应用于所有属性。
    //BrowsableAttribute – 表示是否在网格中显示属性。此特性可用于在网格中隐藏属性。默认情况下，公共属性始终显示在网格中。可以将此特性应用于 SettingsChanged 属性。
    //ReadOnlyAttribute – 表示属性是否为只读。此特性可用于禁止在网格中编辑属性。默认情况下，带有 get 和 set 访问函数的公共属性在网格中是可以编辑的。可以将此特性应用于 AppVersion 属性。
    //DefaultValueAttribute – 表示属性的默认值。如果希望为属性提供默认值，然后确定该属性值是否与默认值相同，则可使用此特性。可以将此特性应用于所有属性。
    //DefaultPropertyAttribute – 表示类的默认属性。在网格中选择某个类时，将首先突出显示该类的默认属性。可以将此特性应用于 AppSettings 类。


    class Config
    {
        [Category("系统") ]
        [DisplayName("系统哦名称")]
        public string AppName { get; set; }
        [DisplayName("机台代码")]
        [Category("系统")]
        public string Host_ID { get; set; }
        [Category("系统")]
        [DisplayName("本机IP")]
        [DefaultValue("127.0.0.1")]
        public string IP { get; set; }
        [Category("系统")]
        [DisplayName("系统版本")]
        public string Version { get; set; }
        [Category("系统")]
        [DisplayName("超时时间")]
        [DefaultValue(200)]
        public double ReadOverTime { get; set; }


        [Category("设置")]
        [DisplayName("COM端口")]
        public string Port { get; set; }
        [DisplayName("信号强度")]
        [DefaultValue(30)]
        [Category("设置")]
        public int? Power { get; set; }
        [Category("设置")]
        [DisplayName("URL地址")] 
        public string Url { get; set; }
        [DisplayName("工序名称")]
        [DefaultValue("激光焊接")]
        [Category("设置")]
        public string ProcessName { get; set; }
        [Category("设置")]
        [DisplayName("工站类型")]
        [DefaultValue("work")]
        public string WRKST_Type { get; set; }
        [Category("设置")]
        [DisplayName("16位EPC")]
        [DefaultValue(16)]
        public int FixEPCLen16 { get; set; }
        [Category("设置")]
        [DefaultValue(24)]
        [DisplayName("24位EPC")]
        public int ProEPCLen24 { get; set; }
        


    }

}
