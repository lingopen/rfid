﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.RFID.Models
{
    public class EPC : IComparable
    {
        public string epc;
        public long count;
        public string devNo;
        public byte antNo;

        int IComparable.CompareTo(object obj)
        {
            return string.Compare(this.epc, ((EPC)obj).epc);
        }
    }
}
