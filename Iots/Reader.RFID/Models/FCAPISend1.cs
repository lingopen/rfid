﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.RFID.Models
{
    public class FCAPISend1
    {
        public string request_id { get; set; }

        public string host_id { get; set; }

        public string host_ip { get; set; }

        public string rfid_tag_sn { get; set; }

        public string code_sn { get; set; }

        public string code_type { get; set; }

        public string bind_type { get; set; }
    }
}