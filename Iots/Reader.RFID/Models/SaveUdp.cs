﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.RFID.Models
{
    class SaveUdp
    {
        public string FixEPC { get; internal set; }
        public string ProEPC { get; internal set; }
        public string EquipNO { get; internal set; }
        public string Res { get; internal set; }
    }
}
