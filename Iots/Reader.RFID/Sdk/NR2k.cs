﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace lingopen.Foxconn.RFID.Sdk
{
    public  class NR2k
    {
        public const int R_OK = 0;
        public const int R_FAIL = -1;

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int ConnectDev(ref int hDev, byte[] host);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int DisconnectDev(ref int hDev);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetDevVersion(int hDev, byte[] pVer);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int BeginInv(int hDev, NR2k.HANDLE_FUN f);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int StopInv(int hDev);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int InvOnce(int hDev, NR2k.HANDLE_FUN f);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetAnt(int hDev, out PANT_CFG pAntCfg);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetAnt(int hDev, ref PANT_CFG pAntCfg);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int WriteTagData(int hDev, uint bank, uint begin, uint size, byte[] pData, byte[] password);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int ReadTagData(int hDev, uint bank, uint begin, uint size, byte[] pData, byte[] password);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int KillTag(int hDev, byte[] kill_pwd, byte[] access_pwd);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int LockTag(int hDev, uint opcode, uint block, byte[] password);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetDeviceNo(int hDev, uint deviceNo);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetNeighJudge(int hDev, byte time);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetDO(int hDev, int port, int state);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetDI(int hDev, byte[] state);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int ResetTagBuffer(int hDev);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int ReadTagBuffer(int hDev, NR2k.HANDLE_FUN f, int readtime);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetClock(int hDev, byte[] pClock);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetClock(int hDev, byte[] pClock);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetBuzzer(int hDev, out byte status);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetBuzzer(int hDev, byte status);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetWorkMode(int hDev, out byte workmode);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetTrigModeDelayTime(int hDev, out byte delaytime);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetWorkMode(int hDev, int workmode);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetTrigModeDelayTime(int hDev, byte delaytime);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetNeighJudge(int hDev, out byte enableNJ, out byte njTime);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetDeviceNo(int hDev, out byte deviceNo);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetDeviceNo(int hDev, byte deviceNo);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetReadZone(int hDev, out byte zone);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetReadZonePara(int hDev, out byte bank, out byte begin, out byte length);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetReadZone(int hDev, byte deviceNo);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetReadZonePara(int hDev, int bank, int begin, int length);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int GetOutputMode(int hDev, out int outputmode);

        [DllImport(@"Sdk\NR2k.dll")]
        public static extern int SetOutputMode(int hDev, int outputmode);

        public delegate void HANDLE_FUN(int hDev, int cmdID, IntPtr pData, int length, IntPtr pHost);
    }
}
