﻿using Reader.RFID7.Model;
using Reader.RFID7.Sdk;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

namespace Reader.RFID7.Forms
{
    public class Form1 : Form
    {
        private static LogWrite lw = new LogWrite();
        private static int iFacCheck = 0;
        private static int iBDKind = 3;
        private static AutoResetEvent autoEvent1 = new AutoResetEvent(false);
        private static AutoResetEvent autoEvent2 = new AutoResetEvent(false);
        private static TagList Tag_data1 = new TagList();
        private static TagList Tag_data2 = new TagList();
        private LogWrite lwError = new LogWrite();
        private NR2k.HANDLE_FUN f = new NR2k.HANDLE_FUN(Form1.HandleData);
        private int[] hDev = new int[1];
        private static int iProEPCLen;
        private static int iFixEPCLen;
        private Form1.TagInfoFucDelegate taginfofuccallback0;
        private Form1.TagInfoFucDelegate taginfofuccallback1;
        private Form1.TagInfoFucDelegate taginfofuccallback2;
        private static System.Timers.Timer timer1;
        private static System.Timers.Timer timer2;
        private static string epc;
        private string sLastProEPC1;
        private string sLastFixEPC1;
        private string sLastProEPC2;
        private string sLastFixEPC2;
        private IContainer components;
        private ListView listView1;
        private ColumnHeader columnHeader1;
        private ColumnHeader columnHeader2;
        private ColumnHeader columnHeader3;
        private Label lbProRes;
        private RadioButton rBtnThreeCode;
        private LogRichTextBox lrtxtLog;
        private RadioButton rBtnTwocode;
        private ListView listView2;
        private ColumnHeader columnHeader5;
        private ColumnHeader columnHeader6;
        private ColumnHeader columnHeader7;
        private ColumnHeader columnHeader9;
        private ColumnHeader columnHeader10;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private Label labelVersion;
        private PictureBox pictureBox1;
        private ColumnHeader columnHeader4;
        private GroupBox groupBox1;
        private Label label1;
        private PictureBox pictureBox2;
        private SerialPort serialPort1;
        private SerialPort serialPort2;
        private ColumnHeader columnHeader8;

        public static event Form1.UpdateControlEventHandler UpdateControl1;

        public Form1()
        {
            this.InitializeComponent();
            this.lwError.DirectoryPath = Environment.CurrentDirectory + "\\Log_Eor";
            this.lwError.FileName = DateTime.Now.ToString("yyyy-MM-dd");
            Form1.lw.DirectoryPath = Environment.CurrentDirectory + "\\Log_DP";
            Form1.lw.FileName = DateTime.Now.ToString("yyyy-MM-dd");
            Form1.iProEPCLen = Convert.ToInt32(ConfigHelper.LoadConfig("ProEPCLen"));
            Form1.iFixEPCLen = Convert.ToInt32(ConfigHelper.LoadConfig("FixEPCLen"));
            Form1.iFacCheck = Convert.ToInt32(ConfigHelper.LoadConfig("FacNo"));
            Form1.iBDKind = Convert.ToInt32(ConfigHelper.LoadConfig("BDKind"));
            Form1.UpdateControl1 = new Form1.UpdateControlEventHandler(this.UpdateListView1);
            Form1.timer1 = new System.Timers.Timer((double)Convert.ToInt32(ConfigHelper.LoadConfig("ReadOverTime")));
            Form1.timer1.Elapsed += new ElapsedEventHandler(this.timer1_Elapsed);
            Form1.timer2 = new System.Timers.Timer((double)Convert.ToInt32(ConfigHelper.LoadConfig("ReadOverTime")));
            Form1.timer2.Elapsed += new ElapsedEventHandler(this.timer2_Elapsed);
            this.taginfofuccallback1 = new Form1.TagInfoFucDelegate(this.TagInfoFunc1);
            this.taginfofuccallback2 = new Form1.TagInfoFucDelegate(this.TagInfoFunc2);
            this.taginfofuccallback0 = new Form1.TagInfoFucDelegate(this.TagInfoFunc0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Form1.iBDKind == 3)
                this.rBtnThreeCode.Checked = true;
            else
                this.rBtnTwocode.Checked = true;
            this.serialPort1.PortName = "COM" + ConfigHelper.LoadConfig("Port1");
            this.serialPort1.BaudRate = int.Parse(ConfigHelper.LoadConfig("Baud1"));
            try
            {
                this.serialPort1.Open();
                this.pictureBox2.BackColor = Color.Green;
                this.label1.Text = "连接正常";
            }
            catch (Exception ex)
            {
                this.WriteLog(this.lrtxtLog, ex.Message, 1);
                this.pictureBox2.BackColor = Color.Red;
                this.label1.Text = "传输串口1连接错误";
            }
            if (Form1.iFacCheck == 1)
            {
                this.serialPort2.PortName = "COM" + ConfigHelper.LoadConfig("Port2");
                this.serialPort2.BaudRate = int.Parse(ConfigHelper.LoadConfig("Baud2"));
                try
                {
                    this.serialPort2.Open();
                    this.pictureBox2.BackColor = Color.Green;
                    this.label1.Text = "连接正常";
                }
                catch (Exception ex)
                {
                    this.WriteLog(this.lrtxtLog, ex.Message, 1);
                    this.pictureBox2.BackColor = Color.Red;
                    this.label1.Text = "传输串口2连接错误";
                }
            }
            byte[] bytes = Encoding.ASCII.GetBytes("COM" + int.Parse(ConfigHelper.LoadConfig("Port")).ToString());
            PANT_CFG pantCfg = new PANT_CFG();
            pantCfg.power = new uint[4];
            uint result;
            uint.TryParse(ConfigHelper.LoadConfig("Power"), out result);
            if (result > 30U)
                result = 30U;
            pantCfg.power[0] = result * 10U;
            pantCfg.power[1] = result * 10U;
            pantCfg.power[2] = result * 10U;
            pantCfg.power[3] = result * 10U;
            byte[] pVer = new byte[32];
            if (NR2k.ConnectDev(ref this.hDev[0], bytes) == 0 && NR2k.GetDevVersion(this.hDev[0], pVer) == 0)
            {
                this.labelVersion.Text = "连接成功";
                this.pictureBox1.BackColor = Color.Green;
                NR2k.BeginInv(this.hDev[0], this.f);
            }
            else
            {
                this.labelVersion.Text = "连接失败";
                this.pictureBox1.BackColor = Color.Red;
            }
        }

        private void UpdateListView1()
        {
            if (!this.InvokeRequired)
            {
                this.listView1.Items.Clear();
                int num = 0;
                for (int index = 0; index < Form1.Tag_data1.Count; ++index)
                {
                    ++num;
                    this.listView1.Items.Add(new ListViewItem()
                    {
                        Text = num.ToString(),
                        SubItems = {
              Form1.Tag_data1[index].epc,
              Form1.Tag_data1[index].antNo.ToString(),
              Form1.Tag_data1[index].count.ToString()
            }
                    });
                }
                for (int index = 0; index < Form1.Tag_data2.Count; ++index)
                {
                    ++num;
                    this.listView1.Items.Add(new ListViewItem()
                    {
                        Text = num.ToString(),
                        SubItems = {
              Form1.Tag_data2[index].epc,
              Form1.Tag_data2[index].antNo.ToString(),
              Form1.Tag_data2[index].count.ToString()
            }
                    });
                }
            }
            else
                this.Invoke((Delegate)new Form1.UpdateControlDelegate(this.UpdateListView1));
        }

        public static void HandleData(int hDev, int cmdID, IntPtr pData, int length, IntPtr pHost)
        {
            Form1.epc = "";
            byte[] destination = new byte[32];
            byte[] numArray = new byte[32];
            if (length <= 2)
                return;
            Marshal.Copy(pData, destination, 0, length);
            Marshal.Copy(pHost, numArray, 0, length);
            if (length == 4)
            {
                string str = string.Empty;
                for (int index = 0; index < length; ++index)
                    str += string.Format("{0:X2} ", (object)destination[index]);
                string strValue = str.Replace(" ", "");
                if (strValue == "01424547")
                {
                    if (Form1.iFacCheck == 1)
                    {
                        Form1.timer1.Start();
                        Form1.lw.AddTimeLine("大族模式：计时1启动");
                    }
                }
                else if (strValue == "01454E44")
                {
                    Form1.lw.AddTimeLine("光电1传输结束");
                    Form1.autoEvent1.Set();
                }
                else if (strValue == "02424547")
                {
                    if (Form1.iFacCheck == 1)
                    {
                        Form1.timer2.Start();
                        Form1.lw.AddTimeLine("大族模式：计时2启动");
                    }
                }
                else if (strValue == "02454E44")
                {
                    Form1.lw.AddTimeLine("光电2传输结束");
                    Form1.autoEvent2.Set();
                }
                Form1.lw.AddTimeLine(strValue);
            }
            else
            {
                for (int index = 0; index <= length - 5; ++index)
                    Form1.epc += string.Format("{0:X2} ", (object)destination[index]);
                Form1.epc = Form1.epc.Replace(" ", "");
                Form1.lw.AddTimeLine(Form1.epc);
                if ((int)destination[length - 1] == 1)
                {
                    EPC_data epcData1 = Form1.Tag_data1.Get_EPC_Data(Form1.epc);
                    int num = Convert.ToInt32(destination[length - 3].ToString(), 10) * 256 + Convert.ToInt32(destination[length - 2].ToString(), 10);
                    if (epcData1 == null)
                    {
                        EPC_data epcData2 = new EPC_data();
                        epcData2.epc = Form1.epc;
                        epcData2.count = (long)num;
                        epcData2.antNo = destination[length - 1];
                        epcData2.devNo = Encoding.Default.GetString(numArray);
                        lock (Form1.Tag_data1)
                            Form1.Tag_data1.Add(epcData2);
                    }
                    else
                        epcData1.count += (long)num;
                }
                if ((int)destination[length - 1] == 2)
                {
                    EPC_data epcData1 = Form1.Tag_data2.Get_EPC_Data(Form1.epc);
                    int num = Convert.ToInt32(destination[length - 3].ToString(), 10) * 256 + Convert.ToInt32(destination[length - 2].ToString(), 10);
                    if (epcData1 == null)
                    {
                        EPC_data epcData2 = new EPC_data();
                        epcData2.epc = Form1.epc;
                        epcData2.count = (long)num;
                        epcData2.antNo = destination[length - 1];
                        epcData2.devNo = Encoding.Default.GetString(numArray);
                        lock (Form1.Tag_data2)
                            Form1.Tag_data2.Add(epcData2);
                    }
                    else
                        epcData1.count += (long)num;
                }
                Form1.UpdateControl1();
            }
        }

        private void WriteLog(LogRichTextBox logRichTxt, string strLog, int nType)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((Delegate)new Form1.WriteLogUnSafe(this.WriteLog), (object)logRichTxt, (object)strLog, (object)nType);
            }
            else
            {
                if (nType == 0)
                    logRichTxt.AppendTextEx(strLog, Color.Indigo);
                else
                    logRichTxt.AppendTextEx(strLog, Color.Red);
                if (logRichTxt.Lines.Length > 50)
                    logRichTxt.Clear();
                logRichTxt.Select(logRichTxt.TextLength, 0);
                logRichTxt.ScrollToCaret();
                this.lwError.AddTimeLine(strLog);
            }
        }

        private void TagInfoFunc0(string sProEPC, string sFixEPC, string sPort)
        {
            this.lbProRes.Visible = true;
            try
            {
                string str = !this.rBtnThreeCode.Checked ? sProEPC + "," : sProEPC + "," + sFixEPC + ",";
                this.serialPort1.WriteLine(str);
                this.AddRowToListView(sFixEPC, sProEPC, "", "OK", sPort);
                this.lbProRes.Text = "OK";
                Form1.lw.AddTimeLine(str);
            }
            catch (Exception ex)
            {
                this.AddRowToListView(sFixEPC, sProEPC, "", "NG", sPort);
                this.lbProRes.Text = "NG";
                this.WriteLog(this.lrtxtLog, ex.Message, 1);
            }
        }

        private void TagInfoFunc1(string sProEPC, string sFixEPC, string sPort)
        {
            this.lbProRes.Visible = true;
            try
            {
                this.serialPort1.WriteLine(sProEPC);
                this.AddRowToListView("", sProEPC, "", "OK", sPort);
                this.lbProRes.Text = "OK";
                Form1.lw.AddTimeLine(sProEPC);
            }
            catch (Exception ex)
            {
                this.AddRowToListView("", sProEPC, "", "NG", sPort);
                this.lbProRes.Text = "NG";
                this.WriteLog(this.lrtxtLog, ex.Message, 1);
            }
            if (!this.rBtnThreeCode.Checked)
                return;
            Thread.Sleep(int.Parse(ConfigHelper.LoadConfig("UpLoadInterval")));
            try
            {
                this.serialPort1.WriteLine(sFixEPC);
                this.AddRowToListView(sFixEPC, "", "", "OK", sPort);
                this.lbProRes.Text = "OK";
                Form1.lw.AddTimeLine(sFixEPC);
            }
            catch (Exception ex)
            {
                this.AddRowToListView(sFixEPC, "", "", "NG", sPort);
                this.lbProRes.Text = "NG";
                this.WriteLog(this.lrtxtLog, ex.Message, 1);
            }
        }

        private void TagInfoFunc2(string sProEPC, string sFixEPC, string sPort)
        {
            this.lbProRes.Visible = true;
            try
            {
                this.serialPort2.WriteLine(sProEPC);
                this.AddRowToListView("", sProEPC, "", "OK", sPort);
                this.lbProRes.Text = "OK";
                Form1.lw.AddTimeLine(sProEPC);
            }
            catch (Exception ex)
            {
                this.AddRowToListView("", sProEPC, "", "NG", sPort);
                this.lbProRes.Text = "NG";
                this.WriteLog(this.lrtxtLog, ex.Message, 1);
            }
            if (!this.rBtnThreeCode.Checked)
                return;
            Thread.Sleep(int.Parse(ConfigHelper.LoadConfig("UpLoadInterval")));
            try
            {
                this.serialPort2.WriteLine(sFixEPC);
                this.AddRowToListView(sFixEPC, "", "", "OK", sPort);
                this.lbProRes.Text = "OK";
                Form1.lw.AddTimeLine(sFixEPC);
            }
            catch (Exception ex)
            {
                this.AddRowToListView(sFixEPC, "", "", "NG", sPort);
                this.lbProRes.Text = "NG";
                this.WriteLog(this.lrtxtLog, ex.Message, 1);
            }
        }

        private void AddRowToListView(string sFixEPC, string sProEPC, string sEquipNO, string webRes, string sSend)
        {
            this.listView2.Items.Add(new ListViewItem((this.listView2.Items.Count + 1).ToString())
            {
                SubItems = {
          sFixEPC,
          sProEPC,
          webRes,
          DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
          sSend
        }
            });
            this.listView2.Items[this.listView2.Items.Count - 1].EnsureVisible();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            NR2k.StopInv(this.hDev[0]);
            Thread.Sleep(50);
            NR2k.DisconnectDev(ref this.hDev[0]);
            if (this.serialPort1.IsOpen)
                this.serialPort1.Close();
            if (!this.serialPort2.IsOpen)
                return;
            this.serialPort2.Close();
        }

        private void timer1_Elapsed(object sender, EventArgs e)
        {
            Form1.timer1.Stop();
            NR2k.SetDO(this.hDev[0], 1, 0);
            Form1.lw.AddTimeLine("计时1触发，发送停止指令");
            Form1.autoEvent1.Reset();
            Thread.Sleep(Convert.ToInt32(ConfigHelper.LoadConfig("StopWaitTime")));
            Form1.autoEvent1.WaitOne(1000);
            Form1.lw.AddTimeLine("光电1开始分析显示数据");
            EPC_data epcData1 = new EPC_data();
            EPC_data epcData2 = new EPC_data();
            string str1 = string.Empty;
            string str2 = string.Empty;
            if (this.rBtnThreeCode.Checked)
            {
                for (int index = 0; index < Form1.Tag_data1.Count; ++index)
                {
                    if (Form1.Tag_data1[index].epc.Length == Form1.iProEPCLen)
                    {
                        if (Form1.Tag_data1[index].count > epcData1.count)
                        {
                            epcData1 = Form1.Tag_data1[index];
                            str1 = epcData1.epc;
                        }
                    }
                    else if (Form1.Tag_data1[index].epc.Length == Form1.iFixEPCLen && Form1.Tag_data1[index].count > epcData2.count)
                    {
                        epcData2 = Form1.Tag_data1[index];
                        str2 = epcData2.epc;
                    }
                }
                if (str1 == string.Empty || str2 == string.Empty)
                {
                    lock (Form1.Tag_data1)
                    {
                        Form1.Tag_data1.Clear();
                        return;
                    }
                }
            }
            else if (this.rBtnTwocode.Checked)
            {
                for (int index = 0; index < Form1.Tag_data1.Count; ++index)
                {
                    if (Form1.Tag_data1[index].epc.Length == Form1.iProEPCLen && Form1.Tag_data1[index].count > epcData1.count)
                    {
                        epcData1 = Form1.Tag_data1[index];
                        str1 = epcData1.epc;
                    }
                }
                if (str1 == string.Empty)
                {
                    lock (Form1.Tag_data1)
                    {
                        Form1.Tag_data1.Clear();
                        return;
                    }
                }
            }
            if (str1 != this.sLastProEPC1)
            {
                this.sLastProEPC1 = str1;
                this.sLastFixEPC1 = str2;
                if (Form1.iFacCheck == 1)
                    this.Invoke((Delegate)this.taginfofuccallback1, (object)str1, (object)str2, (object)"Port1");
                else
                    this.Invoke((Delegate)this.taginfofuccallback0, (object)str1, (object)str2, (object)"Read1");
            }
            lock (Form1.Tag_data1)
                Form1.Tag_data1.Clear();
        }

        private void timer2_Elapsed(object sender, EventArgs e)
        {
            Form1.timer2.Stop();
            NR2k.SetDO(this.hDev[0], 2, 0);
            Form1.lw.AddTimeLine("计时2触发，发送停止指令");
            Form1.autoEvent2.Reset();
            Thread.Sleep(Convert.ToInt32(ConfigHelper.LoadConfig("StopWaitTime")));
            Form1.autoEvent2.WaitOne(1000);
            Form1.lw.AddTimeLine("光电2开始分析显示数据");
            EPC_data epcData1 = new EPC_data();
            EPC_data epcData2 = new EPC_data();
            string str1 = string.Empty;
            string str2 = string.Empty;
            if (this.rBtnThreeCode.Checked)
            {
                for (int index = 0; index < Form1.Tag_data2.Count; ++index)
                {
                    if (Form1.Tag_data2[index].epc.Length == Form1.iProEPCLen)
                    {
                        if (Form1.Tag_data2[index].count > epcData1.count)
                        {
                            epcData1 = Form1.Tag_data2[index];
                            str1 = epcData1.epc;
                        }
                    }
                    else if (Form1.Tag_data2[index].epc.Length == Form1.iFixEPCLen && Form1.Tag_data2[index].count > epcData2.count)
                    {
                        epcData2 = Form1.Tag_data2[index];
                        str2 = epcData2.epc;
                    }
                }
                if (str1 == string.Empty || str2 == string.Empty)
                {
                    lock (Form1.Tag_data2)
                    {
                        Form1.Tag_data2.Clear();
                        return;
                    }
                }
            }
            else if (this.rBtnTwocode.Checked)
            {
                for (int index = 0; index < Form1.Tag_data2.Count; ++index)
                {
                    if (Form1.Tag_data2[index].epc.Length == Form1.iProEPCLen && Form1.Tag_data2[index].count > epcData1.count)
                    {
                        epcData1 = Form1.Tag_data2[index];
                        str1 = epcData1.epc;
                    }
                }
                if (str1 == string.Empty)
                {
                    lock (Form1.Tag_data2)
                    {
                        Form1.Tag_data2.Clear();
                        return;
                    }
                }
            }
            if (str1 != this.sLastProEPC2)
            {
                this.sLastProEPC2 = str1;
                this.sLastFixEPC2 = str2;
                if (Form1.iFacCheck == 1)
                    this.Invoke((Delegate)this.taginfofuccallback2, (object)str1, (object)str2, (object)"Port2");
                else
                    this.Invoke((Delegate)this.taginfofuccallback0, (object)str1, (object)str2, (object)"Read2");
            }
            lock (Form1.Tag_data2)
                Form1.Tag_data2.Clear();
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (Form1.iFacCheck == 1)
                return;
            string str = this.serialPort1.ReadLine();
            if (str.Contains("Read1"))
            {
                Form1.lw.AddTimeLine("世宗模式：计时1启动");
                Form1.timer1.Start();
            }
            if (!str.Contains("Read2"))
                return;
            Form1.lw.AddTimeLine("世宗模式：计时1启动");
            Form1.timer2.Start();
        }

        private void rBtnThreeCode_CheckedChanged(object sender, EventArgs e)
        {
            Form1.iBDKind = 3;
            ConfigHelper.ChangeConfig("BDKind", "3");
        }

        private void rBtnTwocode_CheckedChanged(object sender, EventArgs e)
        {
            Form1.iBDKind = 2;
            ConfigHelper.ChangeConfig("BDKind", "2");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = (IContainer)new Container();
            this.listView1 = new ListView();
            this.columnHeader1 = new ColumnHeader();
            this.columnHeader2 = new ColumnHeader();
            this.columnHeader3 = new ColumnHeader();
            this.columnHeader4 = new ColumnHeader();
            this.rBtnTwocode = new RadioButton();
            this.lbProRes = new Label();
            this.rBtnThreeCode = new RadioButton();
            this.listView2 = new ListView();
            this.columnHeader5 = new ColumnHeader();
            this.columnHeader6 = new ColumnHeader();
            this.columnHeader7 = new ColumnHeader();
            this.columnHeader9 = new ColumnHeader();
            this.columnHeader10 = new ColumnHeader();
            this.columnHeader8 = new ColumnHeader();
            this.groupBox2 = new GroupBox();
            this.groupBox3 = new GroupBox();
            this.groupBox4 = new GroupBox();
            this.labelVersion = new Label();
            this.pictureBox1 = new PictureBox();
            this.groupBox1 = new GroupBox();
            this.label1 = new Label();
            this.pictureBox2 = new PictureBox();
            this.serialPort1 = new SerialPort(this.components);
            this.serialPort2 = new SerialPort(this.components);
            this.lrtxtLog = new LogRichTextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((ISupportInitialize)this.pictureBox1).BeginInit();
            this.groupBox1.SuspendLayout();
            ((ISupportInitialize)this.pictureBox2).BeginInit();
            this.SuspendLayout();
            this.listView1.BackColor = Color.LightGray;
            this.listView1.Columns.AddRange(new ColumnHeader[4]
            {
        this.columnHeader1,
        this.columnHeader2,
        this.columnHeader3,
        this.columnHeader4
            });
            this.listView1.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, (byte)134);
            this.listView1.Location = new Point(6, 89);
            this.listView1.Name = "listView1";
            this.listView1.Size = new Size(517, 181);
            this.listView1.TabIndex = 8;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = View.Details;
            this.columnHeader1.Text = "序号";
            this.columnHeader1.Width = 86;
            this.columnHeader2.Text = "RFID信息";
            this.columnHeader2.Width = 258;
            this.columnHeader3.Text = "天线号";
            this.columnHeader3.Width = 83;
            this.columnHeader4.Text = "读取次数";
            this.columnHeader4.Width = 65;
            this.rBtnTwocode.AutoSize = true;
            this.rBtnTwocode.Location = new Point(41, 98);
            this.rBtnTwocode.Name = "rBtnTwocode";
            this.rBtnTwocode.Size = new Size(74, 21);
            this.rBtnTwocode.TabIndex = 17;
            this.rBtnTwocode.Text = "二码绑定";
            this.rBtnTwocode.UseVisualStyleBackColor = true;
            this.rBtnTwocode.CheckedChanged += new EventHandler(this.rBtnTwocode_CheckedChanged);
            this.lbProRes.AutoSize = true;
            this.lbProRes.Font = new Font("微软雅黑", 42f, FontStyle.Bold, GraphicsUnit.Point, (byte)134);
            this.lbProRes.ForeColor = SystemColors.ActiveCaptionText;
            this.lbProRes.Location = new Point(23, 19);
            this.lbProRes.Name = "lbProRes";
            this.lbProRes.Size = new Size(117, 75);
            this.lbProRes.TabIndex = 16;
            this.lbProRes.Text = "OK";
            this.rBtnThreeCode.AutoSize = true;
            this.rBtnThreeCode.Checked = true;
            this.rBtnThreeCode.Location = new Point(41, 47);
            this.rBtnThreeCode.Name = "rBtnThreeCode";
            this.rBtnThreeCode.Size = new Size(74, 21);
            this.rBtnThreeCode.TabIndex = 10;
            this.rBtnThreeCode.TabStop = true;
            this.rBtnThreeCode.Text = "三码绑定";
            this.rBtnThreeCode.UseVisualStyleBackColor = true;
            this.rBtnThreeCode.CheckedChanged += new EventHandler(this.rBtnThreeCode_CheckedChanged);
            this.listView2.BackColor = Color.LightGray;
            this.listView2.Columns.AddRange(new ColumnHeader[6]
            {
        this.columnHeader5,
        this.columnHeader6,
        this.columnHeader7,
        this.columnHeader9,
        this.columnHeader10,
        this.columnHeader8
            });
            this.listView2.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, (byte)134);
            this.listView2.Location = new Point(6, 276);
            this.listView2.Name = "listView2";
            this.listView2.Size = new Size(693, (int)byte.MaxValue);
            this.listView2.TabIndex = 12;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = View.Details;
            this.columnHeader5.Text = "序号";
            this.columnHeader5.Width = 48;
            this.columnHeader6.Text = "RFID治具码";
            this.columnHeader6.Width = 128;
            this.columnHeader7.Text = "RFID产品码";
            this.columnHeader7.Width = 184;
            this.columnHeader9.Text = "上传状态";
            this.columnHeader9.Width = 65;
            this.columnHeader10.Text = "时间";
            this.columnHeader10.Width = 144;
            this.columnHeader8.Text = "发送口";
            this.columnHeader8.Width = 101;
            this.groupBox2.Controls.Add((Control)this.lbProRes);
            this.groupBox2.Font = new Font("微软雅黑", 9f, FontStyle.Bold, GraphicsUnit.Point, (byte)134);
            this.groupBox2.Location = new Point(529, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new Size(170, 103);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "上传绑定";
            this.groupBox3.Controls.Add((Control)this.rBtnTwocode);
            this.groupBox3.Controls.Add((Control)this.rBtnThreeCode);
            this.groupBox3.Font = new Font("微软雅黑", 9f, FontStyle.Bold, GraphicsUnit.Point, (byte)134);
            this.groupBox3.Location = new Point(529, 121);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new Size(170, 149);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "绑码类型";
            this.groupBox4.Controls.Add((Control)this.labelVersion);
            this.groupBox4.Controls.Add((Control)this.pictureBox1);
            this.groupBox4.Font = new Font("微软雅黑", 9f, FontStyle.Bold, GraphicsUnit.Point, (byte)134);
            this.groupBox4.Location = new Point(292, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new Size(231, 71);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "RFID设备";
            this.labelVersion.AutoSize = true;
            this.labelVersion.Font = new Font("微软雅黑", 12f, FontStyle.Regular, GraphicsUnit.Point, (byte)134);
            this.labelVersion.Location = new Point(114, 33);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new Size(74, 21);
            this.labelVersion.TabIndex = 14;
            this.labelVersion.Text = "连接正常";
            this.pictureBox1.Location = new Point(37, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Size(67, 39);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.groupBox1.Controls.Add((Control)this.label1);
            this.groupBox1.Controls.Add((Control)this.pictureBox2);
            this.groupBox1.Font = new Font("微软雅黑", 9f, FontStyle.Bold, GraphicsUnit.Point, (byte)134);
            this.groupBox1.Location = new Point(6, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(280, 71);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "传输连接";
            this.label1.AutoSize = true;
            this.label1.Font = new Font("微软雅黑", 12f, FontStyle.Regular, GraphicsUnit.Point, (byte)134);
            this.label1.Location = new Point(125, 33);
            this.label1.Name = "label1";
            this.label1.Size = new Size(74, 21);
            this.label1.TabIndex = 16;
            this.label1.Text = "连接正常";
            this.pictureBox2.Location = new Point(48, 22);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Size(67, 39);
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            this.serialPort1.DataReceived += new SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            this.lrtxtLog.Location = new Point(705, 12);
            this.lrtxtLog.Name = "lrtxtLog";
            this.lrtxtLog.Size = new Size(239, 519);
            this.lrtxtLog.TabIndex = 11;
            this.lrtxtLog.Text = "";
            this.AutoScaleDimensions = new SizeF(6f, 12f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(948, 537);
            this.Controls.Add((Control)this.listView1);
            this.Controls.Add((Control)this.groupBox4);
            this.Controls.Add((Control)this.groupBox1);
            this.Controls.Add((Control)this.groupBox3);
            this.Controls.Add((Control)this.groupBox2);
            this.Controls.Add((Control)this.listView2);
            this.Controls.Add((Control)this.lrtxtLog);
            this.FormBorderStyle = FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "富士康激光焊接车间RFID信息绑定应用程序 V2018.10.15.1";
            this.FormClosing += new FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((ISupportInitialize)this.pictureBox1).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((ISupportInitialize)this.pictureBox2).EndInit();
            this.ResumeLayout(false);
        }

        public delegate void DeleConnectDev(byte[] ip);

        public delegate void UpdateControlEventHandler();

        private delegate void UpdateControlDelegate();

        private delegate void TagInfoFucDelegate(string sProEPc, string sFixEPC, string sPort);

        private delegate void WriteLogUnSafe(LogRichTextBox logRichTxt, string strLog, int nType);
    }
}
