﻿namespace Reader.RFID7.Forms
{
    partial class FrmRFID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRFID));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblUpd = new System.Windows.Forms.Label();
            this.lblRfid = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.btnInit = new System.Windows.Forms.ToolStripButton();
            this.btnSetting = new System.Windows.Forms.ToolStripButton();
            this.lbl1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblMachineID = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblPort = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rad24 = new System.Windows.Forms.RadioButton();
            this.rad16 = new System.Windows.Forms.RadioButton();
            this.rad3 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnTest2 = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.listRead = new System.Windows.Forms.ListView();
            this.colIndex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEPC = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colANT = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCOM = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPas = new System.Windows.Forms.GroupBox();
            this.listPass = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblChk = new System.Windows.Forms.GroupBox();
            this.listPost = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.rtxt = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lblPass = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.lblPas.SuspendLayout();
            this.lblChk.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblUpd);
            this.groupBox3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(150, 95);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "上料確認";
            // 
            // lblUpd
            // 
            this.lblUpd.BackColor = System.Drawing.Color.Ivory;
            this.lblUpd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUpd.Font = new System.Drawing.Font("微软雅黑", 32F);
            this.lblUpd.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.lblUpd.Location = new System.Drawing.Point(3, 25);
            this.lblUpd.Name = "lblUpd";
            this.lblUpd.Size = new System.Drawing.Size(144, 67);
            this.lblUpd.TabIndex = 0;
            this.lblUpd.Text = "就緒";
            this.lblUpd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRfid
            // 
            this.lblRfid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRfid.BackColor = System.Drawing.Color.Salmon;
            this.lblRfid.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F);
            this.lblRfid.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRfid.Location = new System.Drawing.Point(1233, 584);
            this.lblRfid.Name = "lblRfid";
            this.lblRfid.Size = new System.Drawing.Size(28, 19);
            this.lblRfid.TabIndex = 0;
            this.lblRfid.Text = "NG";
            this.lblRfid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnInit,
            this.btnSetting,
            this.lbl1,
            this.lblMachineID,
            this.toolStripStatusLabel1,
            this.lblPort});
            this.statusStrip1.Location = new System.Drawing.Point(0, 581);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1283, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // btnInit
            // 
            this.btnInit.Image = ((System.Drawing.Image)(resources.GetObject("btnInit.Image")));
            this.btnInit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnInit.Name = "btnInit";
            this.btnInit.Size = new System.Drawing.Size(60, 28);
            this.btnInit.Text = "绑码";
            this.btnInit.Visible = false;
            this.btnInit.Click += new System.EventHandler(this.btnInit_Click);
            // 
            // btnSetting
            // 
            this.btnSetting.Image = ((System.Drawing.Image)(resources.GetObject("btnSetting.Image")));
            this.btnSetting.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(60, 28);
            this.btnSetting.Text = "设置";
            this.btnSetting.Visible = false;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // lbl1
            // 
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(47, 17);
            this.lbl1.Text = "机台码:";
            // 
            // lblMachineID
            // 
            this.lblMachineID.BackColor = System.Drawing.SystemColors.Info;
            this.lblMachineID.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F);
            this.lblMachineID.Name = "lblMachineID";
            this.lblMachineID.Size = new System.Drawing.Size(90, 17);
            this.lblMachineID.Text = "OP1-L01-0001";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(35, 17);
            this.toolStripStatusLabel1.Text = "端口:";
            // 
            // lblPort
            // 
            this.lblPort.BackColor = System.Drawing.SystemColors.Info;
            this.lblPort.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(45, 17);
            this.lblPort.Text = "COM3";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rad24);
            this.groupBox4.Controls.Add(this.rad16);
            this.groupBox4.Controls.Add(this.rad3);
            this.groupBox4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox4.Location = new System.Drawing.Point(323, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(161, 95);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "綁定類型";
            // 
            // rad24
            // 
            this.rad24.AutoSize = true;
            this.rad24.Checked = true;
            this.rad24.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.rad24.Location = new System.Drawing.Point(19, 67);
            this.rad24.Name = "rad24";
            this.rad24.Size = new System.Drawing.Size(136, 25);
            this.rad24.TabIndex = 0;
            this.rad24.TabStop = true;
            this.rad24.Text = "二碼綁定(24位)";
            this.rad24.UseVisualStyleBackColor = true;
            // 
            // rad16
            // 
            this.rad16.AutoSize = true;
            this.rad16.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.rad16.Location = new System.Drawing.Point(19, 28);
            this.rad16.Name = "rad16";
            this.rad16.Size = new System.Drawing.Size(136, 25);
            this.rad16.TabIndex = 0;
            this.rad16.Text = "二碼綁定(16位)";
            this.rad16.UseVisualStyleBackColor = true;
            this.rad16.Visible = false;
            // 
            // rad3
            // 
            this.rad3.AutoSize = true;
            this.rad3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.rad3.Location = new System.Drawing.Point(187, 25);
            this.rad3.Name = "rad3";
            this.rad3.Size = new System.Drawing.Size(92, 25);
            this.rad3.TabIndex = 0;
            this.rad3.Text = "三碼綁定";
            this.rad3.UseVisualStyleBackColor = true;
            this.rad3.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnTest2);
            this.groupBox2.Controls.Add(this.btnClear);
            this.groupBox2.Controls.Add(this.btnTest);
            this.groupBox2.Controls.Add(this.btnStop);
            this.groupBox2.Controls.Add(this.btnStart);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(490, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(448, 95);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "操作";
            // 
            // btnTest2
            // 
            this.btnTest2.BackColor = System.Drawing.SystemColors.Control;
            this.btnTest2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnTest2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnTest2.Location = new System.Drawing.Point(0, 51);
            this.btnTest2.Name = "btnTest2";
            this.btnTest2.Size = new System.Drawing.Size(198, 38);
            this.btnTest2.TabIndex = 0;
            this.btnTest2.Text = "過站確認測試";
            this.btnTest2.UseVisualStyleBackColor = false;
            this.btnTest2.Visible = false;
            this.btnTest2.Click += new System.EventHandler(this.btnTest2_Click);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnClear.Location = new System.Drawing.Point(204, 22);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(192, 67);
            this.btnClear.TabIndex = 0;
            this.btnClear.Text = "保存並清除記錄";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnTest
            // 
            this.btnTest.BackColor = System.Drawing.SystemColors.Control;
            this.btnTest.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnTest.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnTest.Location = new System.Drawing.Point(0, 21);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(198, 32);
            this.btnTest.TabIndex = 0;
            this.btnTest.Text = "上料確認測試";
            this.btnTest.UseVisualStyleBackColor = false;
            this.btnTest.Visible = false;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnStop.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.btnStop.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnStop.Location = new System.Drawing.Point(105, 22);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(93, 67);
            this.btnStop.TabIndex = 0;
            this.btnStop.Text = "停止";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnStart.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.btnStart.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnStart.Location = new System.Drawing.Point(6, 22);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(93, 67);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "啟動";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox5);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer2.Size = new System.Drawing.Size(1283, 309);
            this.splitContainer2.SplitterDistance = 470;
            this.splitContainer2.TabIndex = 5;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.listRead);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(470, 309);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "掃描記錄";
            // 
            // listRead
            // 
            this.listRead.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colIndex,
            this.colEPC,
            this.colANT,
            this.colCount,
            this.colCOM});
            this.listRead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listRead.Location = new System.Drawing.Point(3, 25);
            this.listRead.Name = "listRead";
            this.listRead.Size = new System.Drawing.Size(464, 281);
            this.listRead.TabIndex = 0;
            this.listRead.UseCompatibleStateImageBehavior = false;
            this.listRead.View = System.Windows.Forms.View.Details;
            // 
            // colIndex
            // 
            this.colIndex.Text = "序號";
            this.colIndex.Width = 80;
            // 
            // colEPC
            // 
            this.colEPC.Text = "EPC";
            this.colEPC.Width = 260;
            // 
            // colANT
            // 
            this.colANT.Text = "ANT";
            // 
            // colCount
            // 
            this.colCount.Text = "讀取次數";
            this.colCount.Width = 80;
            // 
            // colCOM
            // 
            this.colCOM.Text = "端口";
            this.colCOM.Width = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblPas, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblChk, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(809, 309);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblPas
            // 
            this.lblPas.Controls.Add(this.listPass);
            this.lblPas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPas.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblPas.Location = new System.Drawing.Point(3, 157);
            this.lblPas.Name = "lblPas";
            this.lblPas.Size = new System.Drawing.Size(803, 149);
            this.lblPas.TabIndex = 6;
            this.lblPas.TabStop = false;
            this.lblPas.Text = "過站確認";
            // 
            // listPass
            // 
            this.listPass.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader11,
            this.columnHeader12});
            this.listPass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listPass.Location = new System.Drawing.Point(3, 25);
            this.listPass.Name = "listPass";
            this.listPass.Size = new System.Drawing.Size(797, 121);
            this.listPass.TabIndex = 1;
            this.listPass.UseCompatibleStateImageBehavior = false;
            this.listPass.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "序號";
            this.columnHeader1.Width = 80;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "RFID治具碼";
            this.columnHeader3.Width = 0;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "RFID產品碼";
            this.columnHeader4.Width = 260;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "機台碼";
            this.columnHeader5.Width = 260;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "過站狀態";
            this.columnHeader11.Width = 80;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "時間";
            this.columnHeader12.Width = 200;
            // 
            // lblChk
            // 
            this.lblChk.Controls.Add(this.listPost);
            this.lblChk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblChk.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblChk.Location = new System.Drawing.Point(3, 3);
            this.lblChk.Name = "lblChk";
            this.lblChk.Size = new System.Drawing.Size(803, 148);
            this.lblChk.TabIndex = 5;
            this.lblChk.TabStop = false;
            this.lblChk.Text = "上料確認";
            // 
            // listPost
            // 
            this.listPost.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.listPost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listPost.Location = new System.Drawing.Point(3, 25);
            this.listPost.Name = "listPost";
            this.listPost.Size = new System.Drawing.Size(797, 120);
            this.listPost.TabIndex = 1;
            this.listPost.UseCompatibleStateImageBehavior = false;
            this.listPost.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "序號";
            this.columnHeader2.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "RFID治具碼";
            this.columnHeader6.Width = 0;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "RFID產品碼";
            this.columnHeader7.Width = 260;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "機台碼";
            this.columnHeader8.Width = 260;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "確認狀態";
            this.columnHeader9.Width = 80;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "時間";
            this.columnHeader10.Width = 200;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 121);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox7);
            this.splitContainer1.Size = new System.Drawing.Size(1283, 460);
            this.splitContainer1.SplitterDistance = 309;
            this.splitContainer1.TabIndex = 3;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.rtxt);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.groupBox7.Location = new System.Drawing.Point(0, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1283, 147);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "日誌消息";
            // 
            // rtxt
            // 
            this.rtxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rtxt.Location = new System.Drawing.Point(3, 25);
            this.rtxt.Name = "rtxt";
            this.rtxt.ReadOnly = true;
            this.rtxt.Size = new System.Drawing.Size(1277, 119);
            this.rtxt.TabIndex = 0;
            this.rtxt.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox9);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1283, 121);
            this.panel1.TabIndex = 6;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.lblPass);
            this.groupBox9.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox9.Location = new System.Drawing.Point(167, 12);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(150, 95);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "過站確認";
            // 
            // lblPass
            // 
            this.lblPass.BackColor = System.Drawing.Color.Ivory;
            this.lblPass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPass.Font = new System.Drawing.Font("微软雅黑", 32F);
            this.lblPass.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.lblPass.Location = new System.Drawing.Point(3, 25);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(144, 67);
            this.lblPass.TabIndex = 0;
            this.lblPass.Text = "就緒";
            this.lblPass.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmRFID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1283, 603);
            this.Controls.Add(this.lblRfid);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRFID";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RFID Reader";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmReader_FormClosing);
            this.Load += new System.EventHandler(this.FrmReader_Load);
            this.groupBox3.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.lblPas.ResumeLayout(false);
            this.lblChk.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblRfid;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblMachineID;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblUpd;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rad24;
        private System.Windows.Forms.RadioButton rad16;
        private System.Windows.Forms.RadioButton rad3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ToolStripButton btnSetting;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView listRead;
        private System.Windows.Forms.ColumnHeader colIndex;
        private System.Windows.Forms.RichTextBox rtxt;
        private System.Windows.Forms.ColumnHeader colEPC;
        private System.Windows.Forms.ColumnHeader colANT;
        private System.Windows.Forms.ColumnHeader colCount;
        private System.Windows.Forms.ToolStripStatusLabel lbl1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblPort;
        private System.Windows.Forms.ColumnHeader colCOM;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.ToolStripButton btnInit;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnTest2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox lblPas;
        private System.Windows.Forms.ListView listPass;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.GroupBox lblChk;
        private System.Windows.Forms.ListView listPost;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
    }
}