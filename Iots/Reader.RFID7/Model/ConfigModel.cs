﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reader.RFID7.Model
{
    public class ConfigModel
    {
        public int Port { get; set; }//5
        public int Power { get; set; }//30 
        public int FacNo { get; set; } //1, //厂商 1为大族 2为世宗 
        public int Port1 { get; set; } //1,
        public int Baud1 { get; set; } //115200,
        public int Port2 { get; set; } //4,
        public int Baud2 { get; set; }// 115200, 
        public int FixEPCLen { get; set; } //16, //24：传输24位&16位  16：传输16位&16位
        public int ProEPCLen { get; set; }// 24, 
        public int UpLoadInterval { get; set; }// 200, //数据传输间隔，单位毫秒
        public int ReadOverTime { get; set; }// 2500,
        public int StopWaitTime { get; set; }// 200
    }
}
