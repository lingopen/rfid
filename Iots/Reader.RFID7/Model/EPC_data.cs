﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reader.RFID7
{
    public class EPC_data : IComparable
    {
        public string epc{get;set;}
        public long count { get; set; }
        public string rssi { get; set; }
        public string devNo { get; set; }
        public byte antNo { get; set; }
        public int CompareTo(object obj)
        {
            return string.Compare(this.epc, ((EPC_data)obj).epc);
        }
    }
}
