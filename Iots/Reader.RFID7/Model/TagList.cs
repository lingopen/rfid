﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reader.RFID7.Model
{
    public class TagList : List<EPC_data>
    {
        public EPC_data Get_EPC_Data(string sEPC)
        {
            foreach (EPC_data epcData in (List<EPC_data>)this)
            {
                if (epcData.epc == sEPC)
                    return epcData;
            }
            return (EPC_data)null;
        }
    }
}
