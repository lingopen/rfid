﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Reader.RFID7.Sdk
{
    public class PANT_CFG
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] antEnable;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public uint[] dwell_time;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public uint[] power;
    }
}
