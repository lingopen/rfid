﻿namespace Reader.SFC.Forms
{
    partial class FrmBind24
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBind24));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblCount = new System.Windows.Forms.GroupBox();
            this.listUdp = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.rtxt = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gscan = new System.Windows.Forms.GroupBox();
            this.txtEpc = new System.Windows.Forms.TextBox();
            this.txtBarCode = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rad24 = new System.Windows.Forms.RadioButton();
            this.rad16 = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblUpd = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblRfid = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lbl1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblMachineID = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblPort = new System.Windows.Forms.ToolStripStatusLabel();
            this.timDaiji = new System.Windows.Forms.Timer(this.components);
            this.timeout = new System.Windows.Forms.Timer(this.components);
            this.lblRfid2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.lblCount.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gscan.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 121);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lblCount);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox7);
            this.splitContainer1.Size = new System.Drawing.Size(1247, 334);
            this.splitContainer1.SplitterDistance = 224;
            this.splitContainer1.TabIndex = 8;
            // 
            // lblCount
            // 
            this.lblCount.Controls.Add(this.listUdp);
            this.lblCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCount.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lblCount.Location = new System.Drawing.Point(0, 0);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(1247, 224);
            this.lblCount.TabIndex = 6;
            this.lblCount.TabStop = false;
            this.lblCount.Text = "绑码上传      计数:【0】";
            // 
            // listUdp
            // 
            this.listUdp.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader11,
            this.columnHeader12});
            this.listUdp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listUdp.Location = new System.Drawing.Point(3, 25);
            this.listUdp.Name = "listUdp";
            this.listUdp.Size = new System.Drawing.Size(1241, 196);
            this.listUdp.TabIndex = 1;
            this.listUdp.UseCompatibleStateImageBehavior = false;
            this.listUdp.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "序號";
            this.columnHeader1.Width = 80;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "二維碼信息";
            this.columnHeader3.Width = 240;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "標籤EPC編碼";
            this.columnHeader4.Width = 240;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "機台碼";
            this.columnHeader5.Width = 200;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "上傳狀態";
            this.columnHeader11.Width = 140;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "時間";
            this.columnHeader12.Width = 180;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.rtxt);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.groupBox7.Location = new System.Drawing.Point(0, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1247, 106);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "日誌消息";
            // 
            // rtxt
            // 
            this.rtxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rtxt.Location = new System.Drawing.Point(3, 25);
            this.rtxt.Name = "rtxt";
            this.rtxt.ReadOnly = true;
            this.rtxt.Size = new System.Drawing.Size(1241, 78);
            this.rtxt.TabIndex = 0;
            this.rtxt.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gscan);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1247, 121);
            this.panel1.TabIndex = 9;
            // 
            // gscan
            // 
            this.gscan.Controls.Add(this.txtEpc);
            this.gscan.Controls.Add(this.txtBarCode);
            this.gscan.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.gscan.Location = new System.Drawing.Point(354, 9);
            this.gscan.Name = "gscan";
            this.gscan.Size = new System.Drawing.Size(890, 95);
            this.gscan.TabIndex = 0;
            this.gscan.TabStop = false;
            this.gscan.Text = "掃描二維碼";
            // 
            // txtEpc
            // 
            this.txtEpc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEpc.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtEpc.Location = new System.Drawing.Point(6, 60);
            this.txtEpc.Name = "txtEpc";
            this.txtEpc.ReadOnly = true;
            this.txtEpc.Size = new System.Drawing.Size(878, 29);
            this.txtEpc.TabIndex = 0;
            this.txtEpc.TextChanged += new System.EventHandler(this.txtEpc_TextChanged);
            // 
            // txtBarCode
            // 
            this.txtBarCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBarCode.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtBarCode.Location = new System.Drawing.Point(6, 23);
            this.txtBarCode.Name = "txtBarCode";
            this.txtBarCode.Size = new System.Drawing.Size(878, 29);
            this.txtBarCode.TabIndex = 0;
            this.txtBarCode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtBarCode_KeyUp);
            this.txtBarCode.Leave += new System.EventHandler(this.txtBarCode_Leave);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rad24);
            this.groupBox4.Controls.Add(this.rad16);
            this.groupBox4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox4.Location = new System.Drawing.Point(167, 9);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(181, 95);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "绑定类型";
            // 
            // rad24
            // 
            this.rad24.AutoSize = true;
            this.rad24.Checked = true;
            this.rad24.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.rad24.Location = new System.Drawing.Point(19, 41);
            this.rad24.Name = "rad24";
            this.rad24.Size = new System.Drawing.Size(120, 25);
            this.rad24.TabIndex = 0;
            this.rad24.TabStop = true;
            this.rad24.Text = "產品碼(24位)";
            this.rad24.UseVisualStyleBackColor = true;
            // 
            // rad16
            // 
            this.rad16.AutoSize = true;
            this.rad16.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.rad16.Location = new System.Drawing.Point(19, 41);
            this.rad16.Name = "rad16";
            this.rad16.Size = new System.Drawing.Size(120, 25);
            this.rad16.TabIndex = 0;
            this.rad16.Text = "治具碼(16位)";
            this.rad16.UseVisualStyleBackColor = true;
            this.rad16.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblUpd);
            this.groupBox3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox3.Location = new System.Drawing.Point(12, 9);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(150, 95);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "上傳狀態";
            // 
            // lblUpd
            // 
            this.lblUpd.BackColor = System.Drawing.Color.Ivory;
            this.lblUpd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUpd.Font = new System.Drawing.Font("微软雅黑", 32F);
            this.lblUpd.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.lblUpd.Location = new System.Drawing.Point(3, 25);
            this.lblUpd.Name = "lblUpd";
            this.lblUpd.Size = new System.Drawing.Size(144, 67);
            this.lblUpd.TabIndex = 0;
            this.lblUpd.Text = "待機";
            this.lblUpd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblRfid);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(12, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(150, 95);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "RFID設備狀態";
            this.groupBox1.Visible = false;
            // 
            // lblRfid
            // 
            this.lblRfid.BackColor = System.Drawing.Color.Salmon;
            this.lblRfid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRfid.Font = new System.Drawing.Font("微软雅黑", 32F);
            this.lblRfid.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRfid.Location = new System.Drawing.Point(3, 25);
            this.lblRfid.Name = "lblRfid";
            this.lblRfid.Size = new System.Drawing.Size(144, 67);
            this.lblRfid.TabIndex = 0;
            this.lblRfid.Text = "NG";
            this.lblRfid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbl1,
            this.lblMachineID,
            this.toolStripStatusLabel1,
            this.lblPort});
            this.statusStrip1.Location = new System.Drawing.Point(0, 455);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1247, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lbl1
            // 
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(47, 17);
            this.lbl1.Text = "機台碼:";
            // 
            // lblMachineID
            // 
            this.lblMachineID.BackColor = System.Drawing.SystemColors.Info;
            this.lblMachineID.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F);
            this.lblMachineID.Name = "lblMachineID";
            this.lblMachineID.Size = new System.Drawing.Size(90, 17);
            this.lblMachineID.Text = "OP1-L01-0001";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(35, 17);
            this.toolStripStatusLabel1.Text = "端口:";
            // 
            // lblPort
            // 
            this.lblPort.BackColor = System.Drawing.SystemColors.Info;
            this.lblPort.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(45, 17);
            this.lblPort.Text = "COM3";
            // 
            // timDaiji
            // 
            this.timDaiji.Interval = 180000;
            this.timDaiji.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timeout
            // 
            this.timeout.Interval = 3000;
            this.timeout.Tick += new System.EventHandler(this.timeout_Tick);
            // 
            // lblRfid2
            // 
            this.lblRfid2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRfid2.AutoSize = true;
            this.lblRfid2.Location = new System.Drawing.Point(1213, 460);
            this.lblRfid2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRfid2.Name = "lblRfid2";
            this.lblRfid2.Size = new System.Drawing.Size(17, 12);
            this.lblRfid2.TabIndex = 1;
            this.lblRfid2.Text = "NG";
            // 
            // FrmBind24
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1247, 477);
            this.Controls.Add(this.lblRfid2);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmBind24";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "綁碼";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmBind_FormClosing);
            this.Load += new System.EventHandler(this.FrmBind_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.lblCount.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.gscan.ResumeLayout(false);
            this.gscan.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RichTextBox rtxt;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblRfid;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rad24;
        private System.Windows.Forms.RadioButton rad16;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblUpd;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lbl1;
        private System.Windows.Forms.ToolStripStatusLabel lblMachineID;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblPort;
        private System.Windows.Forms.GroupBox lblCount;
        private System.Windows.Forms.ListView listUdp;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.GroupBox gscan;
        private System.Windows.Forms.TextBox txtEpc;
        private System.Windows.Forms.TextBox txtBarCode;
        private System.Windows.Forms.Timer timDaiji;
        private System.Windows.Forms.Timer timeout;
        private System.Windows.Forms.Label lblRfid2;
    }
}