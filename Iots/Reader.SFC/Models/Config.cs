﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.SFC.Models
{


    //DescriptionAttribute - 设置显示在属性下方说明帮助窗格中的属性文本。这是一种为活动属性（即具有焦点的属性）提供帮助文本的有效方法。可以将此特性应用于 MaxRepeatRate 属性。
    //CategoryAttribute - 设置属性在网格中所属的类别。当您需要将属性按类别名称分组时，此特性非常有用。如果没有为属性指定类别，该属性将被分配给杂项 类别。可以将此特性应用于所有属性。
    //BrowsableAttribute – 表示是否在网格中显示属性。此特性可用于在网格中隐藏属性。默认情况下，公共属性始终显示在网格中。可以将此特性应用于 SettingsChanged 属性。
    //ReadOnlyAttribute – 表示属性是否为只读。此特性可用于禁止在网格中编辑属性。默认情况下，带有 get 和 set 访问函数的公共属性在网格中是可以编辑的。可以将此特性应用于 AppVersion 属性。
    //DefaultValueAttribute – 表示属性的默认值。如果希望为属性提供默认值，然后确定该属性值是否与默认值相同，则可使用此特性。可以将此特性应用于所有属性。
    //DefaultPropertyAttribute – 表示类的默认属性。在网格中选择某个类时，将首先突出显示该类的默认属性。可以将此特性应用于 AppSettings 类。

    public class System
    {
        public string AppName { get; set; }//富士康激光焊接车间RFID采集系统", //程序名称
        public string Version { get; set; }//v1.0", //系统版本
        public int ReadOverTime { get; set; }  //上传延迟200毫秒
        public string Port { get; set; }//COM3", //串口号
        public int? Power { get; set; }  //读写天线能信号强度
        public int FixEPCLen16 { get; set; } //治具EPC长度
        public int ProEPCLen24 { get; set; }//产品EPC长度
        //public bool IsDebug { get; set; }//调试模式
        public string AppStart { get; set; }//程序类型
    }
    /// <summary>
    /// 通用
    /// </summary>
    public class Currency
    {
        public string host_id { get; set; }//DAEZZKB013FL0101", //機台編碼
        public string host_ip { get; set; }//127.0.0.1", //IP地址
        public string Url0 { get; set; }//http://10.195.225.59/protoline_webapi/spweld/api_spweld.ashx?action=bindcode", //RFID绑定
        public string Url1 { get; set; }//http://sfc02-icge-gl.ipebg.efoxconn.com:8080/sfc_cg_api/mpline/sfc/do_check", //上料确认
        public string Url2 { get; set; }//http://10.207.237.13:53002/mdline_api/mdline/sfc/do_check", //过站确认
        public string process_name { get; set; }//4017036126", // 制程ID號，由SFC系統自動生成，需富士康提供
        public string wrkst_type { get; set; }//wipin", // 工作类型wipin
        public string wrkst_action { get; set; }//work", // 過站類型，取決富士康系統配置資料，需富士康提供
        public string operation_user { get; set; }//AUTO", // 填入RFID或AUTO，定義過站的掃描方式為讀取RFID標籤或者自動化設備（上料機或者加工機台）自動採集
        public string ProcessType { get; set; }//Assembly", //默認填入Laser_Welding或Assembly，equipment_type為L時，上傳Laser_Welding；為Z時上傳Assembly，必填參數
        public string MachineModule { get; set; }//DAEGLC085FL0101", //填入機台管制編號，必填參數
        public string CodeType { get; set; }//RFID", //產品類型，必填參數（BG/Band/CG/FIXTURE，根據實際加工的產品類型填入默認值）
        public string ProdType { get; set; }//RFID", //默認填入RFID或Barcode，定義prod_code參數為RFID標籤還是產品碼，必填參數
    }
    /// <summary>
    /// 上料确认
    /// </summary>
    public class Feed
    {
        public string prod_type { get; set; }//RFID", //默認填入RFID或Barcode，定義prod_code參數為RFID標籤還是產品碼，必填參數
        public string wrkst_name { get; set; }//開工", // 工站名稱，需富士康提供
    }

    /// <summary>
    /// 綁碼
    /// </summary>
    public class BindingCode
    {
        public int barcode_len { get; set; }//二维码长度
        public int scan_timeout { get; set; }//掃碼超時毫秒
        public string sn_type { get; set; } //绑码类型
        public int PageSize { get; set; }//页面大小
    }
    /// <summary>
    /// RFID过站确认
    /// </summary>
    public class OverStation
    {
        public string machine { get; set; }//GL00HFF551703712", //填入機台內部編號，由富士康提供，必填參數
        public string vendor_id { get; set; }//rtec", //供應商編碼,填rtec
        public string equipment_type { get; set; }//Z", //機台加工類型，默認填入L&Z，L代表鐳射機台，Z代表組裝幾台，必填參數
        public string process_type { get; set; }//Assembly", //默認填入Laser_Welding或Assembly，equipment_type為L時，上傳Laser_Welding；為Z時上傳Assembly，必填參數
        public string machine_module { get; set; }//DAEGLC085FL0101", //填入機台管制編號，必填參數
        public string location { get; set; }//GL-C05-3F", //機台所在位置：廠區-樓層 ，必填參數
        public string line { get; set; }//L001", //產線編號，必填參數
        public string code_type { get; set; }//RFID", //產品類型，必填參數（BG/Band/CG/FIXTURE，根據實際加工的產品類型填入默認值）
        public string work_type { get; set; }//work", //工站類型(sign簽收，bing 綁碼，work 開工，deal 加工)，必填參數
        public string work_action { get; set; }//開工", //填入工站名稱，根據機台定義的路由節點名稱，後臺自動寫入，詳見附件機台與工站名稱對照表，必填參數，對照查詢報裱中station參數
        public string work_trans { get; set; }//deal", //轉向條件(默認 deal)
        public string work_qis { get; set; }//PASS", //填入PASS
        public string process_revs { get; set; }//v1.111" //填入程序版本號
        public string EPC { get; set; }//测试的EPC
        public string BarCode { get; set; }//測試條碼
        public List<BindCode> bind_code { get; set; } //{"code_sn":"XXXXXXXX","code_type":"equipment","trans":"Y","replace":"1"}	//當code_sn為RFID時，trans需填入"Y"；replace填入1時代表不取代主碼過站，填入2時代表取代主碼過站

    }
    public class Config
    {
        public System System { get; set; }
        //系统
        public Currency Currency { get; set; }
        public BindingCode BindingCode { get; set; }
        public Feed Feed { get; set; }
        public OverStation OverStation { get; set; }

        public Config_RFID7 RFID7 { get; set; }
        public List<QR_Type> QR_Types { get; set; }
    }

    public class Config_RFID7
    {

        public string Port1 { get; set; } //1,
        public int Baud1 { get; set; } //115200,
        public string Port2 { get; set; } //4,
        public int Baud2 { get; set; }// 115200,  
        public int UpLoadInterval { get; set; }// 200, //数据传输间隔，单位毫秒
        public int ReadCloseTime { get; set; }// 2500,
        public int StopWaitTime { get; set; }// 200
    }
    public class QR_Type
    {
        public int Key { get; set; }
        public string Value { get; set; }
        public string Prefix { get; set; }
        public string Add { get; set; }
        public string StartWith { get; set; }
    }

}
