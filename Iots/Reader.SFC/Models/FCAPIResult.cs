﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.SFC.Models
{
    public class FCAPIResult
    {
        public string rc { get; set; }

        public string rm { get; set; }

        public string request_id { get; set; }
    }

    public class UpdResult
    {
        /// <summary>
        /// rc 為 000 調用成功, 非000 調用失敗,調用失敗時 rm 中返回異常信息
        /// </summary>
        public string rc { get; set; }
        /// <summary>
        /// ReturnCode 不為 000時 rm 中返回異常信息 ,為 000 時 忽略
        /// </summary>
        public string rm { get; set; }

        public string barcode { get; set; }

        public BarCodeInfo barCodeInfo { get; set; }
        public string request_id { get; set; }
    }
    public class BarCodeInfo
    {
        //"BAR_CODE": "DR8805600D9MNBCAR01",
        //"BILL_NO": "701-1801060F",
        //"BILL_CONFIG": "CONF",
        //"CLIENT_CODE": "",
        //"PRODUCT_ID": "1016034946",
        //"PRODUCT_NAME": "701",
        //"PHASE_ID": "2016035006",
        //"PHASE_NAME": "Proto1",
        //"PART_ID": "3016035326",
        //"PART_NAME": "Housing",
        //"PROCESS_ID": "4017036126",
        //"PROCESS_NAME": "焊接"

         public string BAR_CODE { get; set; }
         public string BILL_NO { get; set; }
        public string BILL_CONFIG { get; set; }
        public string CLIENT_CODE { get; set; }
        public string PRODUCT_ID { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string PHASE_ID { get; set; }
        public string PHASE_NAME { get; set; }
        public string PART_ID { get; set; }
        public string PART_NAME { get; set; }
         public string PROCESS_ID { get; set; }
        public string PROCESS_NAME { get; set; }

    }
}
