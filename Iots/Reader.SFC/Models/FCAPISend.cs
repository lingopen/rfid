﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.SFC.Models
{ 
    public class FCAPISend1
    {
        public string request_id { get; set; }

        public string host_id { get; set; }

        public string host_ip { get; set; }

        public string rfid_tag_sn { get; set; }

        public string code_sn { get; set; }

        public string code_type { get; set; }

        public string bind_type { get; set; }
    }
    public class FCAPISend2
    {
        public string request_id { get; set; }

        public string host_id { get; set; }

        public string host_ip { get; set; }

        public string process_name { get; set; }

        public string wrkst_type { get; set; }

        public string operation_user { get; set; }

        public string part_rfid_tag_sn { get; set; }

        public string fixture_sn { get; set; }

        public string equipment_no { get; set; }
    }
}