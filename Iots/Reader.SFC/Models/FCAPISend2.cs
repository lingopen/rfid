﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.SFC.Models
{
    /// <summary>
    /// 上料确认
    /// </summary>
    public class SFC_Do_Check
    {

        public string request_id { get; set; }//"host_id{get;set;}// //DAEZZKB013FL0101",	// 機台編碼，RFID設備填入SN號
        public string host_id { get; set; }// "10.173.58.176",	// 機台工控機IP，使用IPV4格式 xxx.xxx.xxx.xxx
        public string host_ip { get; set; } //10.176.124.111",	//機台ip,使用IP V4格式 xxx.xxx.xxx.xxx，必填參數
        public string operation_user { get; set; }//"AUTO",	// 填入RFID或AUTO，定義過站的掃描方式為讀取RFID標籤或者自動化設備（上料機或者加工機台）自動採集
        public string process_name { get; set; }// "4017036126",	// 制程ID號，由SFC系統自動生成，需富士康提供
        public string prod_type { get; set; }// RFID",	//填入RFID
        public string wrkst_type { get; set; }//"wipin",	// 填入wipin
        public string wrkst_action { get; set; }// "work",	// 過站類型，取決富士康系統配置資料，需富士康提供
        public string wrkst_name { get; set; }// "開工",	// 工站名稱，需富士康提供
        public string prod_code { get; set; }//: "E2018082801",	// 填入RFID標籤號
        public List<BindCode> bind_code { get; set; }//": [ ],	//保持空值
        public string resv1 { get; set; }// 保留欄位，未使用不需要上傳
        public string resv2 { get; set; }  // 保留欄位，未使用不需要上傳
        public string resv3 { get; set; }	// 保留欄位，未使用不需要上傳


    }
    /// <summary>
    /// 當code_sn為RFID時，trans需填入"Y"；replace填入1時代表不取代主碼過站，填入2時代表取代主碼過站
    /// </summary>
    public class BindCode
    {
        public string code_sn { get; set; } //XXXXXXXX" 當code_sn為RFID時
        public string code_type { get; set; } //equipment
        public string trans { get; set; } //Y
        public string replace { get; set; } //1" 
    }
    public class BindCode2
    {
        public string sn { get; set; } //XXXXXXXX" 當code_sn為RFID時
        public string sn_type { get; set; } //equipment
    }
    public class Parameters_Waveform { }
    public class ProdData
    {
        public ProdData()
        {
            hatch = "1";
            parameters_waveform = new List<Parameters_Waveform>();
            precitec_result = "N/A";
            area = ""; //",	//以下置空值
            cavity = ""; //",
            e_time = ""; //",
            filling_pattern = ""; //",
            frequency = ""; //",
            glue_gap_max = ""; //",
            glue_gap_mean = ""; //",
            glue_gap_min = ""; //",
            glue_gap_result = ""; //",
            glue_miss_result = ""; //",
            glue_width_max = ""; //",
            glue_width_mean = ""; //",
            glue_width_min = ""; //",
            glue_width_result = ""; //",
            glue_width_version = ""; //", 
            humidity = ""; //",
            jumping_speed = ""; //",
            linear_speed = ""; //",
            material = ""; //", 
            pattern_type = ""; //",	
            power = ""; //",	
            pre_push = ""; //",	
            pre_torque = ""; //",	
            precitec_grading = ""; //",	
            precitec_process_rev = ""; //",	 
            precitec_value = ""; //",	
            pulse_energy = ""; //",	
            q_release = ""; //",	
            s_time = ""; //",	
            shim_fall_off = ""; //",	
            spot_size = ""; //",	
            temperature = ""; //"	
        }
        public string area { get; set; } //",	//以下置空值
        public string cavity { get; set; } //",
        public string e_time { get; set; } //",
        public string filling_pattern { get; set; } //",
        public string frequency { get; set; } //",
        public string glue_gap_max { get; set; } //",
        public string glue_gap_mean { get; set; } //",
        public string glue_gap_min { get; set; } //",
        public string glue_gap_result { get; set; } //",
        public string glue_miss_result { get; set; } //",
        public string glue_width_max { get; set; } //",
        public string glue_width_mean { get; set; } //",
        public string glue_width_min { get; set; } //",
        public string glue_width_result { get; set; } //",
        public string glue_width_version { get; set; } //",
        public string hatch { get; set; } //1",
        public string humidity { get; set; } //",
        public string jumping_speed { get; set; } //",
        public string linear_speed { get; set; } //",
        public string material { get; set; } //",
        public List<Parameters_Waveform> parameters_waveform { get; set; }
        public string pattern_type { get; set; } //",	
        public string power { get; set; } //",	
        public string pre_push { get; set; } //",	
        public string pre_torque { get; set; } //",	
        public string precitec_grading { get; set; } //",	
        public string precitec_process_rev { get; set; } //",	
        public string precitec_result { get; set; } //N/A",	
        public string precitec_value { get; set; } //",	
        public string pulse_energy { get; set; } //",	
        public string q_release { get; set; } //",	
        public string s_time { get; set; } //",	
        public string shim_fall_off { get; set; } //",	
        public string spot_size { get; set; } //",	
        public string temperature { get; set; } //"	
    }

    /// <summary>
    /// 过站确认
    /// </summary>
    public class SFC_Do_Pass
    {
        public string request_id { get; set; } //101735817600000000119",	//host_ip+時間（yyyymmddhhmmssfff，時間精確至毫秒）+6碼流水號，host_ip需刪除"."，流水碼000001~999999循環使用，必填參數 
        public string machine { get; set; } //GL00HFF551703712",	//填入機台內部編號，由富士康提供，必填參數
        public string host_id { get; set; } //DAEZZKB013FL0101",	
        public string host_ip { get; set; } //10.176.124.111",	//機台ip,使用IP V4格式 xxx.xxx.xxx.xxx，必填參數
        public string vendor_id { get; set; } //rtec",	//供應商編碼,填rtec
        public string start_time { get; set; } //20180827134435",	//當前讀取標籤時間：yyyymmddhhmmss(24H)  ，必填參數
        public string end_time { get; set; } //20180827134435",	//當前讀取標籤時間：yyyymmddhhmmss(24H) ，必填參數
        public string equipment_type { get; set; } //Z",	//機台加工類型，默認填入L&Z，L代表鐳射機台，Z代表組裝幾台，必填參數
        public string process_type { get; set; } //Assembly",	//默認填入Laser_Welding或Assembly，equipment_type為L時，上傳Laser_Welding；為Z時上傳Assembly，必填參數
        public string machine_module { get; set; } //DAEGLC085FL0101",	//填入機台管制編號，必填參數
        public string location { get; set; } //GL-C05-3F",	//機台所在位置：廠區-樓層 ，必填參數
        public List<BindCode> bind_code { get; set; }
        public string line { get; set; } //L001",	//產線編號，必填參數
        public string code_type { get; set; } //RFID",	//產品類型，必填參數（BG/Band/CG/FIXTURE，根據實際加工的產品類型填入默認值）
        public string prod_barcode { get; set; } //E2018082801",	//條碼，必填參數
        public string prod_type { get; set; } //RFID",	//默認填入RFID或Barcode，定義prod_code參數為RFID標籤還是產品碼，必填參數
        public string process_name { get; set; } //4017036126",	//濕式拋光制程：4015028807；成型1制程：4015028858；成型2制程：4015028773，必填參數
        public string work_type { get; set; } //work",	//工站類型(sign簽收，bing 綁碼，work 開工，deal 加工)，必填參數   
        public string work_action { get; set; } //開工",	//填入工站名稱，根據機台定義的路由節點名稱，後臺自動寫入，詳見附件機台與工站名稱對照表，必填參數，對照查詢報裱中station參數
        public string work_trans { get; set; } //deal",	//轉向條件(默認 deal)
        public string work_qis { get; set; } //PASS",	//填入PASS
        public string process_revs { get; set; } //v1.111",	//填入程序版本號
        public string glue_open_time { get; set; } //",	//間隔時間
        public string vacum_exhaust_pressure { get; set; } //",	//壓力
        public string prod_cnt { get; set; } //",	//空值
        public string product_no { get; set; } //",	//機種，空值
        public string phase_no { get; set; } //",	//階段，空值
        public string bill_no { get; set; } //",	//執行單，空值
        public string config { get; set; } //",	//config，空值
        public string wo_no { get; set; } //",	//工單，空值
        public List<ProdData> prod_data { get; set; }//產品在機台的加工參數，且prod_data欄位為必填參數 

        public string resv1 { get; set; }// 保留欄位，未使用不需要上傳
        public string resv2 { get; set; }  // 保留欄位，未使用不需要上傳
        public string resv3 { get; set; }   // 保留欄位，未使用不需要上傳

    }

    public class SFC_Do_Sign
    {

        public List<BindCode2> bind_code { get; set; }   //產品標籤要綁定的(機台編碼、治具標籤,上料確認時，bind_code欄位參數可為空) ，上料確認此部份保持空值
        public string host_id { get; set; }//Hans005",	/ /請填入機台編碼                                                                                                          必填
        public string host_ip { get; set; }//10.173.58.176",	//機台ip，使用IP V4格式 xxx.xxx.xxx.xxx                                                                                必填
        public string operation_user { get; set; }//AUTO",	// operation_user欄位參數為RFID時填入產品標籤，為AUTO時，填入產品碼                                                    必填
        public string process_name { get; set; }//32312964",	//螺母焊接(32312964)、Frame簽收(7380256) ，可查看製程id對照表                                                          必填
        public string rfid { get; set; }//FR86483L010001QQQQ4A01",	//主條碼                                                                                                   必填
        public string request_id { get; set; }//101735815720180515102043300000031",	//iP地址（1017358176）+時間（20180611152137）+六位流水碼（000001），每次請求流水碼+1           必填
        public string resv1 { get; set; }//",                                                                                                                                           	
        public string resv2 { get; set; }//",                                                                                                                                           	
        public string resv3 { get; set; }//",                                                                                                                                           	
        public string wrkst_action { get; set; }//sign",	// 填入工站：該製程路由對應的工站 wrkst_action（sign 、work、deal）                                                        必填
        public string wrkst_name { get; set; }//簽收",	// 填入工站名稱：該製程路由對應的工站名稱wrkst_name（簽收 、開工、加工）                                                   必填
        public string wrkst_type { get; set; }//wipin"	// Wip In類型，填入wipin                                                                                                   必填

    }
}