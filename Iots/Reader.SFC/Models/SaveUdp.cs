﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reader.SFC.Models
{
    class SaveUdp
    {
        public string FixEPC { get;   set; }
        public string ProEPC { get;   set; }
        public string EquipNO { get;   set; }
        public string Res { get;   set; }
        public string Port { get; set; }
    }
}
