﻿using Core.Com;
using Reader.SFC.Forms;
using Reader.SFC.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reader.SFC
{
    static class Program
    {
        /// <summary>
        /// 
        /// </summary>
        public static Config AppConfig { get; set; }
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            InitConfig();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (AppConfig.System.AppStart == "1") //绑码
                Application.Run(new FrmBind16());
            else if (AppConfig.System.AppStart == "2") //绑码
                Application.Run(new FrmBind24());
            else if(AppConfig.System.AppStart=="71"|| AppConfig.System.AppStart == "72")//上传大族激光机
                Application.Run(new FrmRFID71());
            //else if (AppConfig.System.AppStart == "72")//上传世宗激光机
            //    Application.Run(new FrmRFID72());
            else
                Application.Run(new FrmRFID());
            
        }

        static Config InitConfig()
        {
            //读取配置
            try
            {
                var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Reader.SFC.json");
                if (!File.Exists(path))
                {
                    "Reader.SFC.json文件不存在!".Log();
                    return null;
                }
                else
                {
                    //读取json文件
                    string json = "";
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(path))
                    {
                        json = sr.ReadToEnd();
                    }
                    AppConfig = json.FromJson<Config>();
                    return AppConfig;

                }
            }
            catch (Exception ex)
            {
                $"InitConfig.Error={ex.Message}".Log();
                return null;
            }

        }
    }
}
