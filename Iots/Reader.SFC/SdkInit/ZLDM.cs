﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Reader.SFC.SdkInit
{
    public class ZLDM
    {
        public static ushort m_DevCnt;    // 搜索到的设备数量
        public static byte m_SelectedDevNo = 0;
        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_StartSearchDev")]
        public static extern UInt16 StartSearchDev();

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetVer")]
        public static extern int GetVer();

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetParam")]
        public static extern bool SetParam(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetDevID")]
        public static extern IntPtr GetDevID(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetDevName")]
        public static extern IntPtr GetDevName(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetIPMode")]
        public static extern byte GetIPMode(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetIP")]
        public static extern IntPtr GetIP(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetPort")]
        public static extern ushort GetPort(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetGateWay")]
        unsafe public static extern IntPtr GetGateWay(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetWorkMode")]
        public static extern byte GetWordMode(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetNetMask")]
        public static extern IntPtr GetNetMask(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetDestName")]
        public static extern IntPtr GetDestName(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetDestPort")]
        public static extern ushort GetDestPort(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetBaudrateIndex")]
        public static extern byte GetBaudrateIndex(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetParity")]
        public static extern byte GetParity(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_GetDataBits")]
        public static extern byte GetDataBits(byte nNum);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetDevName")]
        unsafe public static extern bool SetDevName(byte nNum, char* DevName);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetIP")]
        unsafe public static extern bool SetIP(byte nNum, byte[] IP);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetGateWay")]
        unsafe public static extern bool SetGateWay(byte nNum, byte[] GateWay);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetNetMask")]
        unsafe public static extern bool SetNetMask(byte nNum, byte[] NetMask);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetDestName")]
        unsafe public static extern bool SetDestName(byte nNum, byte[] DestName);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetIPMode")]
        public static extern bool SetIPMode(byte nNum, byte IPMode);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetPort")]
        public static extern bool SetPort(byte nNum, ushort Port);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetWorkMode")]
        public static extern bool SetWorkMode(byte nNum, byte WorkMode);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetDestPort")]
        public static extern bool SetDestPort(byte nNum, ushort DestPort);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetBaudrateIndex")]
        public static extern bool SetBaudrateIndex(byte nNum, byte BaudrateIndex);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetParity")]
        public static extern bool SetParity(byte nNum, byte Parity);

        [DllImport(@"SdkInit\ZlDevManage.dll", EntryPoint = "ZLDM_SetDataBits")]
        public static extern bool SetDataBits(byte nNum, byte DataBits);

    }
}
