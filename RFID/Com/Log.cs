﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lingopen.Foxconn.RFID.Com
{
    public class RichTextBoxLog
    {
        public delegate void LogAppendDelegate(Color color, string text);
        RichTextBox richTextBoxRemote;

        /// <summary>
        /// 构造函数传入RichTextBox控件的实例。
        /// </summary>
        /// <param name="richTextBoxRemote"></param>
        public RichTextBoxLog(RichTextBox richTextBoxRemote)
        {
            this.richTextBoxRemote = richTextBoxRemote;
        }

        /// <summary>
        /// LogAppendDelegate委托指向的方法
        /// </summary>
        /// <param name="color"></param>
        /// <param name="text"></param>
        private void LogAppend(Color color, string text)
        {
            text.Log();
            richTextBoxRemote.SelectionColor = color;
            richTextBoxRemote.AppendText(text);
            richTextBoxRemote.AppendText(System.Environment.NewLine);
            richTextBoxRemote.ScrollToCaret();
        }

        /// <summary>   
        /// 追加显示文本   
        /// </summary>   
        /// <param name="text"></param>   
        public void LogAppendMsg(string text)
        {
            text.Log();
            LogAppendDelegate la = new LogAppendDelegate(LogAppend);
            richTextBoxRemote.Invoke(la, Color.Black, DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff]:") + text);
        }

        /// <summary>   
        /// 显示错误日志   
        /// </summary>   
        /// <param name="text"></param>   
        public void LogError(string text)
        {
            text.Log();
            LogAppendDelegate la = new LogAppendDelegate(LogAppend);
            richTextBoxRemote.Invoke(la, Color.Red, DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff]:") + text);
        }
        /// <summary>   
        /// 显示警告信息   
        /// </summary>   
        /// <param name="text"></param>   
        public void LogWarning(string text)
        {
            text.Log();
            LogAppendDelegate la = new LogAppendDelegate(LogAppend);
            richTextBoxRemote.Invoke(la, Color.Violet, DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff]:") + text);
        }
        /// <summary>   
        /// 显示信息   
        /// </summary>   
        /// <param name="text"></param>   
        public void LogMessage(string text)
        {
            text.Log();
            LogAppendDelegate la = new LogAppendDelegate(LogAppend);
            richTextBoxRemote.Invoke(la, Color.Black, DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff]:") + text);
        }

    }



    /// <summary>
    /// 本地日志记录
    /// </summary>
    public static class LocalLog
    {
        //int _hashCode = 0;
        ///// <summary>
        ///// 构造一个hashcode参数的本地日志对象
        ///// </summary>
        ///// <param name="hashCode"></param>
        //public LocalLog(int hashCode)
        //{
        //    _hashCode = hashCode;
        //}
        private static string writeLock = "";
        /// <summary>
        /// 记录本地日志文件
        /// </summary>
        /// <param name="content">内容</param>
        public static void Log(this string content)
        {
            lock (writeLock)
            {
                var time = DateTime.Now;
                var dirPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Logs", time.ToString("yyyyMM"));
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
                //半小时一个日志文件
                string filePath = Path.Combine(dirPath, time.ToString("yyyyMMddHH") + ".log");
                using (StreamWriter sw = new StreamWriter(filePath, true, Encoding.UTF8))
                {
                    /*var pid = _hashCode; System.Diagnostics.Process.GetCurre
                    ntProcess().Id;*/
                    Console.WriteLine(time.ToString("[yyyy-MM-dd HH:mm:ss.fff]") + content);
                    sw.WriteLine(time.ToString("[yyyy-MM-dd HH:mm:ss.fff]") + content);
                }
            }
        }
    }
}