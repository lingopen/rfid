﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lingopen.Foxconn.RFID.Models
{
    public class FCAPIResult
    {
        public string rc { get; set; }

        public string rm { get; set; }

        public string request_id { get; set; }
    }
}
